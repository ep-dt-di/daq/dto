# DTO - Data Transfer Orchestrator
Software to move data produced by data acquisition (DAQ) systems to permanent storage.

## Documentation
The documentation can be found in the [doc/](doc/) directory.


