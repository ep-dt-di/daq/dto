#!/usr/bin/env python3
#
# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import sys

from config import load_configuration_file, validate_configuration
from controllers.DtoController import DtoController
from controllers.EosController import EosController
from controllers.FtsController import FtsController
from models.utils import get_db_session

if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    logging.info('starting script')
    if not load_configuration_file():
        sys.exit(1)
    if not validate_configuration():
        sys.exit(1)

    session = get_db_session(os.environ.get('db_user'),
                             os.environ.get('db_pass'),
                             os.environ.get('db_host'),
                             os.environ.get('db_port'),
                             os.environ.get('db_name')
                            )()
    host = os.environ.get('host')
    # host = "na62primitive"
    fts_instance = FtsController(os.environ.get('endpoint'))
    eos_storage = EosController(endpoint=os.environ.get('jump_protocol_base'))

    dto = DtoController(fts_instance, host, eos_storage)
    dto.do_overwrite(session, amount=10)
