# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import unittest

import controllers.SourceController as SourceControllerClass


class TestSourceControllerRouting(unittest.TestCase):

    def test_destination(self):
        host = 'myhost.domain.ch'
        source_dir = '/merger/cdr'

        regex = r'na62raw_([0-9]*)-([0-9]*)-([0-9]*)-([0-9]*).*\.dat'
        regex_group_index = 3

        # test 1
        file_name = 'na62raw_1494236504-02-007058-0007.dat'
        new_source = SourceControllerClass.SourceController(host, source_dir + '/' + file_name)

        self.assertEqual(new_source.route_with_regex('/castor/cern.ch/na62/data/2017/raw/prerun',
                                                     regex,
                                                     regex_group_index
                                                    ),
                         '/castor/cern.ch/na62/data/2017/raw/prerun/007058'
                        )

        # test 2
        file_name = 'na62raw_0-02-007058-0007.dat'
        new_source = SourceControllerClass.SourceController(host, source_dir + '/' + file_name)
        self.assertEqual(new_source.route_with_regex('/castor/cern.ch/na62/data/2017/raw/prerun',
                                                     regex,
                                                     regex_group_index
                                                    ),
                         '/castor/cern.ch/na62/data/2017/raw/prerun/007058'
                        )

        # test 3
        file_name = 'na62raw_0-02-000000-3166.dat'
        new_source = SourceControllerClass.SourceController(host, source_dir + '/' + file_name)
        self.assertEqual(new_source.route_with_regex('/castor/cern.ch/na62/data/2017/raw/prerun',
                                                     regex,
                                                     regex_group_index
                                                    ),
                         '/castor/cern.ch/na62/data/2017/raw/prerun/000000'
                        )

        # test 4
        file_name = 'list.txt'
        new_source = SourceControllerClass.SourceController(host, source_dir + '/' + file_name)
        self.assertEqual(new_source.route_with_regex('/castor/cern.ch/na62/data/2017/raw/prerun',
                                                     regex,
                                                     regex_group_index
                                                    ),
                         '/castor/cern.ch/na62/data/2017/raw/prerun'
                        )

        # test 5
        file_name = 'calib_scan'
        new_source = SourceControllerClass.SourceController(host, source_dir + '/' + file_name)
        self.assertEqual(new_source.route_with_regex('/castor/cern.ch/na62/data/2017/raw/prerun',
                                                     regex,
                                                     regex_group_index
                                                    ),
                         '/castor/cern.ch/na62/data/2017/raw/prerun'
                        )

        ##Test negative regex index check
        file_name = 'list.txt'
        new_source = SourceControllerClass.SourceController(host, source_dir + '/' + file_name)
        self.assertEqual(new_source.route_with_regex('/castor/cern.ch/na62/data/2017/raw/prerun',
                                                     'whatever',
                                                     -1
                                                    ),
                         '/castor/cern.ch/na62/data/2017/raw/prerun'
                        )

def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestSourceControllerRouting, 'test'))
    return test_suite

if __name__ == '__main__':
    unittest.main()
