import unittest

from tests.unit import test_source_controller_routing
from tests.unit import test_dto_controller

def usuite():

    test_suite = unittest.TestSuite()
    test_suite.addTest(test_source_controller_routing.suite())
    test_suite.addTest(test_dto_controller.suite())

    return test_suite

def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(usuite())
    return test_suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite', verbosity=2)
