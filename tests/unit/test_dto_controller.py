# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import unittest
from unittest import mock

from controllers.DtoController import DtoController


class TestDtoController(unittest.TestCase):

    def setUp(self):
        self.dto = DtoController(fts_controller=mock.MagicMock(), host=mock.MagicMock())
        self.dto.logger = mock.MagicMock()

    def test_get_new_files(self):
        with mock.patch('controllers.SourceController') as tmp_mock:
            file_dir = ''
            self.assertEqual(self.dto._get_new_files(file_dir), [])

    def test_submit_fts_submit_exception(self):
        self.dto.fts_instance = mock.MagicMock()
        self.dto.fts_instance.submit = mock.MagicMock(side_effect=Exception)
        session = mock.MagicMock()
        source = mock.MagicMock()
        with self.assertRaises(Exception):
            self.dto.submit(session, source, None)

    def test_do_record_session_exception(self):
        session = mock.MagicMock()
        session.add = mock.MagicMock(side_effect=Exception)
        source = mock.MagicMock()
        with self.assertRaises(Exception):
            self.dto._do_record(session, source)
        session.rollback.assert_called_once()

    def test_resubmit_record_exception(self):
        self.dto._do_record = mock.MagicMock(side_effect=Exception)
        session = mock.MagicMock()
        source = mock.MagicMock()
        record = mock.MagicMock()
        with self.assertRaises(Exception):
            self.dto._resubmit(session, source, record)

    def test_resubmit_session_exception(self):
        self.dto.submit = mock.MagicMock(return_value='1abc')
        session = mock.MagicMock()
        session.add = mock.MagicMock(side_effect=Exception)
        source = mock.MagicMock()
        with self.assertRaises(Exception):
            self.dto._resubmit(session, source, None)
            session.rollback.assert_called_once()


def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestDtoController, 'test'))
    return test_suite


if __name__ == '__main__':
    unittest.main()
