# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import unittest
from datetime import datetime, timedelta
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from alembic import command
from alembic.config import Config

from models.Transfer import Transfer
from controllers.DtoController import DtoController
from tests.helpers.FakeFtsController import FakeFtsController

class TestDtoControllerFetchStatus(unittest.TestCase):
    alembic_cfg = Config('./ci/alembic-test.ini')

    @classmethod
    def setUpClass(cls):
        command.upgrade(cls.alembic_cfg, 'head')

        # reusing the same configuration stored in the alembic file
        db_url_string = cls.alembic_cfg.get_main_option('sqlalchemy.url')
        engine = create_engine(db_url_string, echo=False)
        session_func = sessionmaker(bind=engine)
        cls.session = session_func()

        cls.fts_instance = FakeFtsController('http://myendpoint.ch')
        cls.dto = DtoController(cls.fts_instance, 'myhostname.domain.ch')

    @classmethod
    def tearDownClass(cls):
        # If you forgot to close the session alembic will not be able to downgrade
        cls.session.close()
        command.downgrade(cls.alembic_cfg, 'base')

    def setUp(self):
        old_transfer = Transfer(id=1,
                                # Not nullable no default
                                src_path='/root',
                                file_name='file1.dat',
                                file_size=30,
                                # useful for the test
                                status='SUBMITTED',
                                src_host='myhostname.domain.ch',
                                job_id='job1-aaa-bbbb-cccc-dddd-eeeeeeeeeeee',
                                updated_at=datetime(2017, 11, 28, 10, 55, 25))
        recent_transfer = Transfer(id=2,
                                   # Not nullable no default
                                   src_path='/root',
                                   file_name='file2.dat',
                                   file_size=40,
                                   # useful for the test
                                   status='SUBMITTED',
                                   src_host='myhostname.domain.ch',
                                   job_id='job2-aaa-bbbb-cccc-dddd-eeeeeeeeeeee',
                                   updated_at=datetime(2020, 1, 4, 12, 40, 10))
        invalid_transfer = Transfer(id=3,
                                    # Not nullable no default
                                    src_path='/root',
                                    file_name='file3.dat',
                                    file_size=45,
                                    # useful for the test
                                    status='SUBMITTED',
                                    src_host='myhostname.domain.ch',
                                    job_id='job3-aaa-bbbb-cccc-dddd-eeeeeeeeeeee',
                                    updated_at=datetime(2020, 1, 5, 16, 10, 35),
                                    is_invalid=1)
        self.session.add_all([old_transfer, recent_transfer, invalid_transfer])
        self.session.commit()

    def tearDown(self):
        # clean up database
        self.session.query(Transfer).delete()


    def test_oldest_updated_first(self):
        date_now = datetime.utcnow()
        delta = timedelta(minutes=2)

        self.dto.fetch_limit = 1 # set fetch limit to 1
        # A status that not trigger any status change for the transfer
        self.fts_instance.statuses = ['NEUTRAL_STATUS']
        self.fts_instance.weights = [100]

        #########
        # first transfer
        (ok, ko) = self.dto.fetch_transfer_statuses(self.session)
        # Transfer is not FINISHED nor FAILED
        self.assertEqual(ok + ko, 0)

        # fetching the first record from the db
        records = self.session.query(Transfer).order_by(Transfer.id.asc())

        # test status not changed
        self.assertEqual(records[0].status, 'SUBMITTED')
        # test updated_at updated in utc format
        self.assertGreater(records[0].updated_at, date_now - delta)
        self.assertLess(records[0].updated_at, date_now + delta)
        self.assertEqual(records[0].transferred_at, None)

        # test more recent entry not updated
        self.assertEqual(records[1].status, 'SUBMITTED')
        self.assertEqual(records[1].updated_at, datetime(2020, 1, 4, 12, 40, 10))
        self.assertEqual(records[1].transferred_at, None)

        #########
        # second transfer
        (ok, not_ok) = self.dto.fetch_transfer_statuses(self.session)
        # Transfer is not FINISHED nor FAILED
        self.assertEqual(ok + ko, 0)

        # fetching the record from the db
        records = self.session.query(Transfer).filter_by(id=2)

        # test status not updated
        self.assertEqual(records[0].status, 'SUBMITTED')
        # test updated_at updated in utc format
        self.assertGreater(records[0].updated_at, date_now - delta)
        self.assertLess(records[0].updated_at, date_now + delta)
        self.assertEqual(records[0].transferred_at, None)


    def test_finished_status(self):
        date_now = datetime.utcnow()
        delta = timedelta(minutes=2)

        self.dto.fetch_limit = 1 # set fetch limit to 1
        self.fts_instance.statuses = ['FINISHED']
        self.fts_instance.weights = [100]

        #########
        # first transfer
        (ok, ko) = self.dto.fetch_transfer_statuses(self.session)
        # test that only one transfer has been updated
        self.assertEqual(ok, 1)
        self.assertEqual(ko, 0)

        # fetching the records from the db
        records = self.session.query(Transfer).order_by(Transfer.id.asc())

        # test status updated
        self.assertEqual(records[0].status, 'FINISHED')
        # test updated_at updated in utc format
        self.assertGreater(records[0].updated_at, date_now - delta)
        self.assertLess(records[0].updated_at, date_now + delta)
        self.assertGreater(records[0].transferred_at, date_now - delta)
        self.assertLess(records[0].transferred_at, date_now + delta)

        # test status not updated
        self.assertEqual(records[1].status, 'SUBMITTED')
        self.assertEqual(records[1].updated_at, datetime(2020, 1, 4, 12, 40, 10))
        self.assertEqual(records[1].transferred_at, None)

    def test_failed_status(self):
        date_now = datetime.utcnow()
        delta = timedelta(minutes=2)

        self.dto.fetch_limit = 1 # set fetch limit to 1
        self.fts_instance.statuses = ['FAILED']
        self.fts_instance.weights = [100]

        #########
        # first transfer
        (ok, ko) = self.dto.fetch_transfer_statuses(self.session)
        # test that only one transfer has been updated
        self.assertEqual(ok, 0)
        self.assertEqual(ko, 1)

        # fetching the records from the db
        records = self.session.query(Transfer).order_by(Transfer.id.asc())

        # test status updated
        self.assertEqual(records[0].status, 'FAILED')
        # test updated_at updated in utc format
        self.assertGreater(records[0].updated_at, date_now - delta)
        self.assertLess(records[0].updated_at, date_now + delta)
        self.assertGreater(records[0].transferred_at, date_now - delta)
        self.assertLess(records[0].transferred_at, date_now + delta)

        # test status not updated
        self.assertEqual(records[1].status, 'SUBMITTED')
        self.assertEqual(records[1].updated_at, datetime(2020, 1, 4, 12, 40, 10))
        self.assertEqual(records[1].transferred_at, None)


    def test_canceled_status(self):
        """
        Same as failed, canceled is threated in the same way
        """
        date_now = datetime.utcnow()
        delta = timedelta(minutes=2)

        self.dto.fetch_limit = 1 # set fetch limit to 1
        self.fts_instance.statuses = ['CANCELED']
        self.fts_instance.weights = [100]

        #########
        # first transfer
        (ok, ko) = self.dto.fetch_transfer_statuses(self.session)
        # test that only one transfer has been updated
        self.assertEqual(ok, 0)
        self.assertEqual(ko, 1)

        # fetching the records from the db
        records = self.session.query(Transfer).order_by(Transfer.id.asc())

        # test status updated
        self.assertEqual(records[0].status, 'FAILED')
        # test updated_at updated in utc format
        self.assertGreater(records[0].updated_at, date_now - delta)
        self.assertLess(records[0].updated_at, date_now + delta)
        self.assertGreater(records[0].transferred_at, date_now - delta)
        self.assertLess(records[0].transferred_at, date_now + delta)

        # test status updated
        self.assertEqual(records[1].status, 'SUBMITTED')
        self.assertEqual(records[1].updated_at, datetime(2020, 1, 4, 12, 40, 10))
        self.assertEqual(records[1].transferred_at, None)

    def test_invalid_transfer_ignored(self):
        date_now = datetime.utcnow()
        delta = timedelta(minutes=2)

        self.dto.fetch_limit = 10 # set fetch limit to 10
        self.fts_instance.statuses = ['FINISHED']
        self.fts_instance.weights = [100]

        #########
        # first transfer
        (ok, ko) = self.dto.fetch_transfer_statuses(self.session)
        # test that only one transfer has been updated
        self.assertEqual(ok, 2)
        self.assertEqual(ko, 0)

        # fetching the records from the db
        records = self.session.query(Transfer).order_by(Transfer.id.asc())

        # test status updated
        self.assertEqual(records[0].status, 'FINISHED')
        # test updated_at updated in utc format
        self.assertGreater(records[0].updated_at, date_now - delta)
        self.assertLess(records[0].updated_at, date_now + delta)
        self.assertGreater(records[0].transferred_at, date_now - delta)
        self.assertLess(records[0].transferred_at, date_now + delta)

        self.assertEqual(records[1].status, 'FINISHED')
        # test updated_at updated in utc format
        self.assertGreater(records[1].updated_at, date_now - delta)
        self.assertLess(records[1].updated_at, date_now + delta)
        self.assertGreater(records[1].transferred_at, date_now - delta)
        self.assertLess(records[1].transferred_at, date_now + delta)

        # test status not updated for the invalid transfer
        self.assertEqual(records[2].status, 'SUBMITTED')
        self.assertEqual(records[2].updated_at, datetime(2020, 1, 5, 16, 10, 35))
        self.assertEqual(records[2].transferred_at, None)

def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestDtoControllerFetchStatus, 'test'))
    return test_suite

if __name__ == '__main__':
    unittest.main()
