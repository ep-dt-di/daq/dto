import unittest

from tests.functional import test_transfer_model

from tests.functional import test_source_controller
from tests.functional import test_dto_controller_delete
from tests.functional import test_dto_controller_get_new_files
from tests.functional import test_dto_controller_submit
from tests.functional import test_dto_controller_record
from tests.functional import test_dto_controller_resubmit
from tests.functional import test_dto_controller_fetch_status

def fsuite():

    test_suite = unittest.TestSuite()
    test_suite.addTest(test_transfer_model.suite())
    test_suite.addTest(test_source_controller.suite())
    test_suite.addTest(test_dto_controller_fetch_status.suite())
    test_suite.addTest(test_dto_controller_delete.suite())
    test_suite.addTest(test_dto_controller_get_new_files.suite())
    test_suite.addTest(test_dto_controller_submit.suite())
    test_suite.addTest(test_dto_controller_resubmit.suite())
    test_suite.addTest(test_dto_controller_record.suite())

    return test_suite

def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(fsuite())
    return test_suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite', verbosity=2)
