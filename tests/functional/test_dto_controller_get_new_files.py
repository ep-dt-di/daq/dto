# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import unittest
from unittest import mock

from controllers.DtoController import DtoController
from controllers.SourceController import SourceController
from tests.helpers.FakeFtsController import FakeFtsController
from tests.helpers import utils


class TestDtoControllerGetNewFiles(unittest.TestCase):
    """
    Test DTO controller _get_new_files _mark_as_recorded functions
    Those tests rely only on the filesystem
    """
    source_dir = '/root/dto_functional_tests'

    @classmethod
    def setUpClass(cls):
        # faking the FTS calls
        cls.fts_instance = FakeFtsController('http://myendpoint.ch')
        cls.dto = DtoController(cls.fts_instance, 'myhostname.domain.ch')

        cls.mock_env = mock.patch.dict(os.environ, {
            "source_owner": "dto",
            "source_group": "root",
            "new_source_group": "dto"
        })
        cls.mock_env.start()

    @classmethod
    def tearDownClass(cls):
        cls.mock_env.stop()

    def setUp(self):
        if not os.path.isdir(self.source_dir):
            os.mkdir(self.source_dir)

    def tearDown(self):
        try:
            if os.path.isdir(self.source_dir):
                shutil.rmtree(self.source_dir)
        except Exception as ex:
            pass

    def test_mark_as_recorded(self):
        filename = "%s/test1.txt" % self.source_dir
        with open(filename, "w") as new_file:
            pass

        recorded_object = SourceController('myhostname.domain.ch', filename)
        self.dto._mark_as_recorded(recorded_object)
        self.assertEqual(recorded_object.get_group(), "dto")

    def test_new_empty_file(self):
        filename = "%s/test1.txt" % self.source_dir
        with open(filename, "w") as new_file:
            pass
            # Empty file

        # file is marked as not finished must not be submitted
        result = self.dto._get_new_files(self.source_dir)
        self.assertEqual(len(result), 0)

        # Test that the file with size 0 still exist
        file_exist = os.path.exists(filename)
        self.assertTrue(file_exist)

        # File produced from the DAQ is completed
        utils.set_owner(filename, 'dto')

        # File is empty must not be submitted
        result = self.dto._get_new_files(self.source_dir)
        self.assertEqual(len(result), 0)

        # Test that the file with size 0 is deleted
        file_exist = os.path.exists(filename)
        self.assertFalse(file_exist)

    def test_new_file(self):
        filename = "%s/test2.txt" % self.source_dir
        with open(filename, "w") as new_file:
            new_file.write("[DTO testing] This file is used by an automated test!")

        # file is marked as not finished must not be submitted
        result = self.dto._get_new_files(self.source_dir)
        self.assertEqual(len(result), 0)

        # File produced from the DAQ is completed
        utils.set_owner(filename, 'dto')

        result = self.dto._get_new_files(self.source_dir)
        self.assertEqual(len(result), 1)

        self.assertIsInstance(result[0], SourceController)

        # mark the file as recorded
        self.dto._mark_as_recorded(result[0])

        # a file must not be indexed twice
        result = self.dto._get_new_files(self.source_dir)
        self.assertEqual(len(result), 0)

    def test_get_new_files_limit(self):
        for index in range(12):
            filename = "%s/test%d.txt" % (self.source_dir, index)
            with open(filename, 'w') as afile:
                afile.write('[DTO testing] This file is used by an automated test!')
                # File produced from the DAQ is completed
                utils.set_owner(filename, 'dto')

        file_objects = self.dto._get_new_files(self.source_dir)
        self.assertEqual(len(file_objects), 10)

        # mark the files as recorded
        for file_object in file_objects:
            self.dto._mark_as_recorded(file_object)

        # Second iteration finds the two remaining ones
        result = self.dto._get_new_files(self.source_dir)
        self.assertEqual(len(result), 2)

def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestDtoControllerGetNewFiles, 'test'))
    return test_suite

if __name__ == '__main__':
    unittest.main(verbosity=2)
