# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import os
import pwd
import shutil
import logging
import unittest

from controllers.SourceController import SourceController

class TestSourceController(unittest.TestCase):
    """Name pattern: test_<name_of_the_tested_function>_<short_description_test_case>"""

    source_dir = '/tmp/dto_functional_tests'

    valid_base_name = "valid_file.dat"

    valid_file_path = "%s/%s" % (source_dir, valid_base_name)
    empty_file_path = "%s/%s" % (source_dir, "empty_file.dat")
    inexistent_file_path = "%s/%s" % (source_dir, "non_existent_file.dat")

    # change CRITICAL to ERROR or INFO in order to enable logging during testing
    logging.basicConfig(
        level=logging.CRITICAL,
        format='%(asctime)s - %(name)s (%(lineno)s) - %(levelname)s: %(message)s',
        datefmt='%Y.%m.%d %H:%M:%S')
    logger = logging.getLogger(__name__)

    def setUp(self):
        if not os.path.isdir(self.source_dir):
            os.mkdir(self.source_dir)

        # Not existing file
        self.inexistent_file = SourceController('myhost.domain.ch', self.inexistent_file_path)

        # Empty file
        with open(self.empty_file_path, 'w') as afile:
            pass
        self.empty_file = SourceController('myhost.domain.ch', self.empty_file_path)

        # Valid file
        with open(self.valid_file_path, 'w') as afile:
            afile.write("[DTO testing] This file is used by an automated test!")
        self.valid_file = SourceController('myhost.domain.ch', self.valid_file_path)

    def tearDown(self):
        try:
            if os.path.isdir(self.source_dir):
                shutil.rmtree(self.source_dir)
        except Exception as ex:
            pass

    def test_get_source_valid_file(self):
        self.assertEqual(self.valid_file.get_source(), self.valid_file_path)

    def test_get_source_empty_file(self):
        self.assertEqual(self.empty_file.get_source(), self.empty_file_path)

    def test_get_owner_valid_file(self):
        self.assertEqual(self.valid_file.get_owner(), "root")

    def test_get_owner_inexistent_file(self):
        self.assertEqual(self.inexistent_file.get_owner(), "nobody")

    def test_get_owner_None_file(self):
        none_file = SourceController('myhost.domain.ch', None)

        result = none_file.get_owner()
        self.assertEqual(result, "nobody")

    def test_get_group_valid_file(self):
        self.assertEqual(self.valid_file.get_group(), "root")

    def test_get_group_inexistent_file(self):
        self.assertEqual(self.inexistent_file.get_group(), "nogroup")

    def test_get_group_None_file(self):
        none_file = SourceController('myhost.domain.ch', None)

        result = none_file.get_group()
        self.assertEqual(result, "nogroup")

    def test_set_group_valid_file_valid_owner(self):
        # TODO What is this test about?
        # Why I do chown before?
        uid = pwd.getpwnam('bin').pw_uid
        gid = os.stat(self.valid_file.source).st_gid
        os.chown(self.valid_file.source, uid, gid)

        self.valid_file.set_group('bin')

        result = self.valid_file.get_owner()
        self.assertEqual(result, 'bin')

    def test_set_group_valid_file_invalid_owner(self):
        with self.assertRaises(KeyError) as context:
            self.valid_file.set_group('99999')

    def test_set_group_valid_file_invalid_string_owner(self):
        with self.assertRaises(KeyError) as context:
            self.valid_file.set_group('.')

    def test_set_group_valid_file_non_existent_owner(self):
        with self.assertRaises(KeyError) as context:
            self.valid_file.set_group('nonexistent')

    def test_set_group_inexistent_file(self):
        with self.assertRaises(OSError) as context:
            self.inexistent_file.set_group('dteam')

    def test_set_group_None_file(self):
        none_file = SourceController('myhost.domain.ch', None)

        with self.assertRaises(TypeError) as context:
            none_file.set_group('dteam')

    def test_get_dir_name_valid_file(self):
        self.assertEqual(self.valid_file.get_dir_name(), self.source_dir)

    def test_get_base_name_valid_file(self):
        self.assertEqual(self.valid_file.get_base_name(), self.valid_base_name)

    def test_get_size_valid_file(self):
        self.assertGreater(self.valid_file.get_size(), 0)

    def test_get_size_empty_file(self):
        self.assertEqual(self.empty_file.get_size(), 0)

    def test_is_size_zero_valid_file(self):
        self.assertFalse(self.valid_file.is_size_zero())

    def test_is_size_zero_empty_file(self):
        self.assertTrue(self.empty_file.is_size_zero())

    def test_delete_valid_file(self):
        file_exist = os.path.exists(self.valid_file_path)
        self.assertTrue(file_exist)

        self.assertTrue(self.valid_file.delete())

        file_exist = os.path.exists(self.valid_file_path)
        self.assertFalse(file_exist)

    def test_delete_inexistent_file(self):
        self.assertFalse(self.inexistent_file.delete())

    def test_rename_valid_file(self):
        self.assertTrue(self.valid_file.rename())

        new_file_exist = os.path.exists("%s/%s.del" % (self.source_dir, self.valid_base_name))
        self.assertTrue(new_file_exist)

        old_file_exist = os.path.exists(self.valid_file_path)
        self.assertFalse(old_file_exist)

    def test_rename_inexistent_file(self):
        self.assertFalse(self.inexistent_file.rename())

    def test_exists_valid_file(self):
        self.assertTrue(self.valid_file.exists())

    def test_exists_inexistent_file(self):
        self.assertFalse(self.inexistent_file.exists())

def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestSourceController, 'test'))
    return test_suite


if __name__ == '__main__':
    unittest.main()
