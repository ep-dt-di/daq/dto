# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import unittest
from unittest import mock

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from alembic import command
from alembic.config import Config

from controllers.DtoController import DtoController
from controllers.SourceController import SourceController
from models.Transfer import Transfer
from tests.helpers.FakeFtsController import FakeFtsController
from tests.helpers.FakeStorageController import FakeStorageController


class TestDtoControllerResubmit(unittest.TestCase):

    alembic_cfg = Config('./ci/alembic-test.ini')
    source_dir = '/root/dto_functional_tests'

    @classmethod
    def setUpClass(cls):
        command.upgrade(cls.alembic_cfg, 'head')

        # reusing the same configuration stored in the alembic file
        db_url_string = cls.alembic_cfg.get_main_option('sqlalchemy.url')
        engine = create_engine(db_url_string, echo=False)
        session_func = sessionmaker(bind=engine)
        cls.session = session_func()

        cls.fts_instance = FakeFtsController('http://myendpoint.ch', statuses=['FAILED'])
        cls.eos_storage = FakeStorageController('http://myendpoint.ch')
        cls.dto = DtoController(cls.fts_instance,
                                'myhostname.domain.ch',
                                eos_storage=cls.eos_storage)

        cls.mock_env = mock.patch.dict(os.environ, {
            'regex_group_index': '-1',
            'base_destination': '/test/destination',
            'protocol_base': 'https://test.cern.ch/dto/cern.ch',
            'jump_base_destination': '/test/destination',
            'jump_protocol_base': 'http://myendpoint.ch',
            'new_source_group': 'dto',
            'source_owner': 'root',
            'source_group': 'root'
        })
        cls.mock_env.start()

    @classmethod
    def tearDownClass(cls):
        # If you forgot to close the session alembic will not be able to downgrade
        cls.session.close()
        command.downgrade(cls.alembic_cfg, 'base')

        cls.mock_env.stop()

    def setUp(self):
        if not os.path.isdir(self.source_dir):
            os.mkdir(self.source_dir)

    def tearDown(self):
        # clean up database
        self.session.query(Transfer).delete()

        try:
            if os.path.isdir(self.source_dir):
                shutil.rmtree(self.source_dir)
        except Exception as ex:
            #print('Cannot remove the directory')
            pass

    def test_resubmit_valid_file_check_recorded(self):
        """
        What is this test about?
        """
        with open(self.source_dir + '/test1.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.dto.host, "%s/test1.txt" % self.source_dir)
        record = self.dto._do_record(self.session, source)
        result = self.dto._submit(self.session, source, record)

        job_id = result[0]

        records = (
            self.session.query(Transfer)
            .filter_by(job_id=job_id)
        )

        new_record_id = self.dto._resubmit(self.session, source, records[0])

        old_record = (
            self.session.query(Transfer)
            .filter_by(job_id=job_id)
        )[0]

        new_record = (
            self.session.query(Transfer)
            .filter_by(id=new_record_id)
        )[0]

        self.assertEqual(new_record.status, 'RECORDED')
        self.assertEqual(new_record.transfer_attempts, 1)
        self.assertEqual(old_record.resubmit_id, new_record.id)

    def test_resubmit_valid_file_check_picked(self):
        """
        What is this test about?
        """
        with open(self.source_dir + '/test1.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.dto.host, "%s/test1.txt" % self.source_dir)
        record = self.dto._do_record(self.session, source)
        result = self.dto._submit(self.session, source, record)

        job_id = result[0]

        records = self.session.query(Transfer).filter_by(job_id=job_id)

        self.dto._resubmit(self.session, source, records[0])
        result = self.dto.submit_files(self.session)

        self.assertEqual(result, (1, 0))

    def test_do_resubmit_valid_files_clean_on_the_jump_server(self):
        """
        Testing the resubmission mechanism simulating a failure between the host and
        the jump destination.
        When resubmiting the file on the jump must not be deleted
        """
        # prepataration
        # preparing 3 database entry for the test
        attempt = 1
        for index in range(3):
            with open(self.source_dir + '/test' + str(index) + '.txt', 'w') as afile:
                afile.write('[DTO testing] This file is used by an automated test!')
            source = SourceController(self.dto.host,
                                      self.source_dir + "/test" + str(index) + ".txt")
            new_transfer = Transfer(
                src_host=self.dto.host,
                src_path=source.get_dir_name(),
                file_name=source.get_base_name(),
                status='FAILED',
                jump_path='/data', # I have to set the jump path
                file_size=source.get_size(),
                job_id='aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee' + str(index),
                transfer_attempts=attempt
            )
            self.session.add(new_transfer)
            self.session.flush()
        self.session.commit()
        # preparation done
        # default behaviour of the storage controller: file exist on the jump

        # I put a negative number otherwise the condition in the query might not be triggered
        # because too fast!
        count = self.dto.do_resubmit(self.session, minutes=-1)
        self.assertEqual(count, 3)

        records = self.session.query(Transfer).filter_by(status='FAILED')

        # we assume that files existed on the jump server
        for record in records:
            self.assertEqual(record.is_jump_file_deleted, 1)
            self.assertNotEqual(record.jump_file_deleted_at, None)
            self.assertEqual(record.transfer_attempts, 1)

        records = self.session.query(Transfer).filter_by(status='RECORDED')
        self.assertEqual(records.count(), 3)

    def test_do_resubmit_valid_files_no_clean_on_the_jump_server(self):
        """
        Testing the resubmission mechanism simulating a failure between the host and
        the jump destination.
        When resubmiting the file on the jump must not be deleted
        """
        # prepataration
        # preparing 3 database entry for the test
        attempt = 1
        for index in range(3):
            with open(self.source_dir + '/test' + str(index) + '.txt', 'w') as afile:
                afile.write('[DTO testing] This file is used by an automated test!')
            source = SourceController(self.dto.host,
                                      self.source_dir + "/test" + str(index) + ".txt")
            new_transfer = Transfer(
                src_host=self.dto.host,
                src_path=source.get_dir_name(),
                file_name=source.get_base_name(),
                status='FAILED',
                jump_path='/data', # I have to set the jump path
                file_size=source.get_size(),
                job_id='aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee' + str(index),
                transfer_attempts=attempt
            )
            self.session.add(new_transfer)
            self.session.flush()
        self.session.commit()

        # Alter behaviour of the storage controller: file does NOT exist on the jump
        eos_storage = FakeStorageController('http://myendpoint.ch', return_values=[False])
        dto = DtoController(self.fts_instance, 'myhostname.domain.ch', eos_storage)
        # preparation done

        # I put a negative number otherwise the condition in the query might not be triggered
        # because too fast!
        count = dto.do_resubmit(self.session, minutes=-1)
        self.assertEqual(count, 3)

        records = self.session.query(Transfer).filter_by(status='FAILED')
        # we assume that files didn't exist on the jump server
        for record in records:
            self.assertEqual(record.is_jump_file_deleted, 0)
            self.assertEqual(record.jump_file_deleted_at, None)
            self.assertEqual(record.transfer_attempts, 1)

        records = self.session.query(Transfer).filter_by(status='RECORDED')
        self.assertEqual(records.count(), 3)

    def test_do_resubmit_valid_files_no_jump_server(self):
        """
        Testing resubmission when jump server is not used
        """
        # prepataration
        # preparing 3 database entry for the test
        attempt = 1
        for index in range(3):
            with open(self.source_dir + '/test' + str(index) + '.txt', 'w') as afile:
                afile.write('[DTO testing] This file is used by an automated test!')
            source = SourceController(self.dto.host,
                                      self.source_dir + "/test" + str(index) + ".txt")
            new_transfer = Transfer(
                src_host=self.dto.host,
                src_path=source.get_dir_name(),
                file_name=source.get_base_name(),
                status='FAILED',
                jump_path=None, # Jump path not set
                file_size=source.get_size(),
                job_id='aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee' + str(index),
                transfer_attempts=attempt
            )
            self.session.add(new_transfer)
            self.session.flush()
        self.session.commit()
        # preparation done

        # I put a negative number otherwise the condition in the query might not be triggered
        # because too fast!
        count = self.dto.do_resubmit(self.session, minutes=-1)
        self.assertEqual(count, 3)

        records = self.session.query(Transfer).filter_by(status='RECORDED')
        self.assertEqual(records.count(), 3)

        for record in records:
            self.assertEqual(record.is_jump_file_deleted, 0)
            self.assertEqual(record.jump_file_deleted_at, None)

    def test_do_resubmit_missing_files(self):
        """
        When a file is deleted by a human interaction have to be invalidated
        """
        # prepataration
        # preparing 3 database entry for the test
        for index in range(3):
            with open(self.source_dir + '/test' + str(index) + '.txt', 'w') as afile:
                afile.write('[DTO testing] This file is used by an automated test!')
            source = SourceController(self.dto.host,
                                      self.source_dir + "/test" + str(index) + ".txt")
            new_transfer = Transfer(
                src_host=self.dto.host,
                src_path=source.get_dir_name(),
                file_name=source.get_base_name(),
                status='FAILED',
                jump_path='/data', # I have to set the jump path
                file_size=source.get_size(),
                job_id='aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee' + str(index),
            )
            self.session.add(new_transfer)
            self.session.flush()
        self.session.commit()

        # remove one file
        os.remove(self.source_dir + "/test1.txt")
        # preparation done

        # I put a negative number otherwise the condition in the query might not be triggered
        # because too fast!
        count = self.dto.do_resubmit(self.session, minutes=-1)
        self.assertEqual(count, 2)

        invalid_records = (
            self.session.query(Transfer)
            .filter_by(src_host=self.dto.host)
            .filter_by(is_invalid=1)
        )
        self.assertEqual(invalid_records.count(), 1)


def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestDtoControllerResubmit, 'test'))
    return test_suite


if __name__ == '__main__':
    unittest.main()
