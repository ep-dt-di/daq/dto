# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import unittest
from unittest import mock

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from alembic import command
from alembic.config import Config

from controllers.DtoController import DtoController
from controllers.SourceController import SourceController
from models.Transfer import Transfer
from tests.helpers.FakeFtsController import FakeFtsController
from tests.helpers import utils


class TestDtoControllerRecord(unittest.TestCase):
    """
    Test DTO controller "record" functions
    """
    alembic_cfg = Config('./ci/alembic-test.ini')
    source_dir = '/root/dto_functional_tests'

    @classmethod
    def setUpClass(cls):
        command.upgrade(cls.alembic_cfg, 'head')

        # faking the FTS calls
        cls.fts_instance = FakeFtsController('http://myendpoint.ch')
        cls.dto = DtoController(cls.fts_instance, 'myhostname.domain.ch')

        # reusing the same configuration stored in the alembic file
        db_url_string = cls.alembic_cfg.get_main_option('sqlalchemy.url')
        engine = create_engine(db_url_string, echo=False)
        session_func = sessionmaker(bind=engine)
        cls.session = session_func()

        cls.mock_env = mock.patch.dict(os.environ, {
            "source_owner": "dto",
            "source_group": "root",
            "new_source_group": "dto",
            "jump_base_destination": "",
            "jump_protocol_base": "",
            "regex_group_index": "-1"
        })
        cls.mock_env.start()

    @classmethod
    def tearDownClass(cls):
        cls.mock_env.stop()

        # If you forgot to close the session alembic will not be able to downgrade
        cls.session.close()
        command.downgrade(cls.alembic_cfg, 'base')

    def setUp(self):
        if not os.path.isdir(self.source_dir):
            os.mkdir(self.source_dir)

    def tearDown(self):
        self.session.query(Transfer).delete()
        try:
            if os.path.isdir(self.source_dir):
                shutil.rmtree(self.source_dir)
        except Exception as ex:
            pass

    def test_do_record_function(self):
        filename = "%s/a_file.dat" % self.source_dir
        with open(filename, "w"):
            pass
        a_file = SourceController(self.dto.host, filename)
        database_entry = self.dto._do_record(self.session, a_file)

        self.assertIsInstance(database_entry, Transfer)

        records = self.session.query(Transfer).all()
        self.assertEqual(records, [database_entry])

        self.assertEqual(records[0].file_name, 'a_file.dat')
        self.assertEqual(records[0].src_host, 'myhostname.domain.ch')
        self.assertEqual(records[0].src_path, self.source_dir)
        self.assertEqual(records[0].dst_path, None)
        self.assertEqual(records[0].status, 'RECORDED')
        self.assertEqual(records[0].job_id, None)
        self.assertEqual(records[0].file_size, 0)
        self.assertEqual(records[0].is_src_deleted, 0)
        self.assertEqual(records[0].transfer_attempts, 0)
        self.assertEqual(records[0].resubmit_id, None)
        self.assertEqual(records[0].is_invalid, 0)
        self.assertEqual(records[0].is_on_tape, 0)
        self.assertEqual(records[0].comment, None)
        self.assertEqual(records[0].jump_path, None)
        self.assertEqual(records[0].is_jump_file_deleted, 0)
        # Many test are already present in test_transfer_model.py

    def test_record_files_empty_directory(self):
        result = self.dto.record_files(self.session, self.source_dir)
        self.assertEqual(result, (0, 0))

    def test_record_files_empty_file(self):
        filename = "%s/test1.txt" % self.source_dir
        with open(filename, "w"):
            # File produced from the DAQ is completed
            utils.set_owner(filename, 'dto')

        result = self.dto.record_files(self.session, self.source_dir)
        self.assertEqual(result, (0, 0))

    def test_record_files_valid_file(self):
        filename = "%s/test2.txt" % self.source_dir
        with open(filename, "w") as new_file:
            new_file.write("[DTO testing] This file is used by an automated test!")
            # File produced from the DAQ is completed
            utils.set_owner(filename, 'dto')

        result = self.dto.record_files(self.session, self.source_dir)
        self.assertEqual(result, (1, 0))

def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestDtoControllerRecord, 'test'))
    return test_suite

if __name__ == '__main__':
    unittest.main(verbosity=2)
