# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import unittest
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from alembic import command
from alembic.config import Config

from controllers.DtoController import DtoController
from controllers.SourceController import SourceController
from models.Transfer import Transfer
from tests.helpers.FakeFtsController import FakeFtsController
from tests.helpers.FakeStorageController import FakeStorageController


class TestDtoControllerDelete(unittest.TestCase):
    alembic_cfg = Config('./ci/alembic-test.ini')
    source_dir = u'/root/dto_functional_tests'

    @classmethod
    def setUpClass(cls):
        command.upgrade(cls.alembic_cfg, 'head')

        # reusing the same configuration stored in the alembic file
        db_url_string = cls.alembic_cfg.get_main_option('sqlalchemy.url')
        engine = create_engine(db_url_string, echo=False)
        session_func = sessionmaker(bind=engine)
        cls.session = session_func()

        cls.fts_instance = FakeFtsController('http://myendpoint.ch')
        cls.eos_storage = FakeStorageController('http://myendpoint.ch')
        cls.dto = DtoController(cls.fts_instance, 'myhostname.domain.ch', cls.eos_storage)

    @classmethod
    def tearDownClass(cls):
        # If you forgot to close the session alembic will not be able to downgrade
        cls.session.close()
        command.downgrade(cls.alembic_cfg, 'base')

    def setUp(self):
        if not os.path.isdir(self.source_dir):
            os.mkdir(self.source_dir)

    def tearDown(self):
        # clean up database
        self.session.query(Transfer).delete()
        self.session.commit()

        try:
            if os.path.isdir(self.source_dir):
                shutil.rmtree(self.source_dir)
        except Exception as ex:
            pass

    def test_delete_files_valid_files(self):
        attempt = 1
        for index in range(3):
            with open(self.source_dir + '/test' + str(index) + '.txt', 'w') as afile:
                afile.write('[DTO testing] This file is used by an automated test!')
            source = SourceController(self.dto.host, "%s/test%d.txt" % (self.source_dir, index))
            new_transfer = Transfer(
                src_host=self.dto.host,
                src_path=source.get_dir_name(),
                file_name=source.get_base_name(),
                status='FINISHED',
                file_size=source.get_size(),
                job_id=u'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee%d' % index,
                transfer_attempts=attempt
            )
            self.session.add(new_transfer)
            self.session.flush()
        self.session.commit()


        result = self.dto.delete_files(self.session, life_in_minutes=0, check_tape=0)
        self.assertEqual(result, 3)

        for index in range(3):
            result = os.path.isfile("%s/test%d.txt" % (self.source_dir, index))
            self.assertFalse(result)

    def test_clean_jump_server_success_FINISHED_file(self):
        with open(self.source_dir + '/test.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.dto.host, "%s/test.txt" % self.source_dir)
        new_transfer = Transfer(
            src_host=self.dto.host,
            src_path=source.get_dir_name(),
            file_name=source.get_base_name(),
            status='FINISHED',
            file_size=source.get_size(),
            created_at=datetime(2017, 11, 28, 10, 55, 25),
            job_id=u'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
            transfer_attempts=1,
            jump_path='/home/dteam/unittest'
        )
        self.session.add(new_transfer)
        self.session.flush()
        self.session.commit()

        self.dto.cleanup_jump_server(self.session, 0)

        record = self.session.query(Transfer).all()[0]
        self.assertEqual(record.is_jump_file_deleted, 1)
        self.assertNotEqual(record.jump_file_deleted_at, None)

    def test_clean_jump_server_success_FINISHED_file_empty_jump_path(self):
        with open(self.source_dir + '/test.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.dto.host, "%s/test.txt" % self.source_dir)
        new_transfer = Transfer(
            src_host=self.dto.host,
            src_path=source.get_dir_name(),
            file_name=source.get_base_name(),
            status='FINISHED',
            file_size=source.get_size(),
            created_at=datetime(2017, 11, 28, 10, 55, 25),
            job_id=u'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
            transfer_attempts=1
        )
        self.session.add(new_transfer)
        self.session.flush()
        self.session.commit()

        self.dto.cleanup_jump_server(self.session, 0)

        record = self.session.query(Transfer).all()[0]
        self.assertEqual(record.is_jump_file_deleted, 0)
        self.assertEqual(record.jump_file_deleted_at, None)

    def test_clean_jump_server_fail(self):
        with open(self.source_dir + '/test.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.dto.host, "%s/test.txt" % self.source_dir)
        new_transfer = Transfer(
            src_host=self.dto.host,
            src_path=source.get_dir_name(),
            file_name=source.get_base_name(),
            status='FINISHED',
            file_size=source.get_size(),
            created_at=datetime(2017, 11, 28, 10, 55, 25),
            job_id=u'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee',
            transfer_attempts=1,
            jump_path='/home/dteam/unittest'
        )
        self.session.add(new_transfer)
        self.session.flush()
        self.session.commit()

        eos_storage = FakeStorageController('http://myendpoint.ch', return_values=[False])
        dto = DtoController(self.fts_instance, 'myhostname.domain.ch', eos_storage)
        dto.cleanup_jump_server(self.session, 0)

        record = self.session.query(Transfer).all()[0]
        self.assertEqual(record.is_jump_file_deleted, 0)
        self.assertEqual(record.jump_file_deleted_at, None)

    def test_delete_files_mixed(self):
        attempt = 1
        statuses = ['FINISHED', 'SUBMITTED', 'FINISHED']

        for index in range(3):
            with open(self.source_dir + '/test' + str(index) + '.txt', 'w') as afile:
                afile.write('[DTO testing] This file is used by an automated test!')
            source = SourceController(self.dto.host, "%s/test%d.txt" % (self.source_dir, index))
            new_transfer = Transfer(
                src_host=self.dto.host,
                src_path=source.get_dir_name(),
                file_name=source.get_base_name(),
                status=statuses[index],
                file_size=source.get_size(),
                job_id=u'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee%d' % index,
                transfer_attempts=attempt
            )
            self.session.add(new_transfer)
            self.session.flush()
        self.session.commit()

        result = self.dto.delete_files(self.session, life_in_minutes=0, check_tape=0)
        self.assertEqual(result, 2)

    def test_delete_files_limit(self):
        for index in range(120):
            with open(self.source_dir + '/test' + str(index) + '.txt', 'w') as afile:
                afile.write('[DTO testing] This file is used by an automated test!')
            source = SourceController(self.dto.host, "%s/test%d.txt" % (self.source_dir, index))
            new_transfer = Transfer(
                src_host=self.dto.host,
                src_path=source.get_dir_name(),
                file_name=source.get_base_name(),
                status='FINISHED',
                file_size=source.get_size(),
                job_id=u'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee%d' % index,
            )
            self.session.add(new_transfer)
            #self.session.flush()
        self.session.commit()

        result = self.dto.delete_files(self.session, life_in_minutes=0, check_tape=0)
        self.assertEqual(result, 100)

        no_files = len(os.listdir(self.source_dir))
        self.assertEqual(no_files, 20)


def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestDtoControllerDelete, 'test'))
    return test_suite


if __name__ == '__main__':
    unittest.main()
