# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import unittest
from datetime import datetime, timedelta
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from alembic import command
from alembic.config import Config

from models.Transfer import Transfer

class TestTransferModelClass(unittest.TestCase):
    alembic_cfg = Config('./ci/alembic-test.ini')

    @classmethod
    def setUpClass(cls):
        command.upgrade(cls.alembic_cfg, 'head')
        # reusing the same configuration stored in the alembic file
        db_url_string = cls.alembic_cfg.get_main_option('sqlalchemy.url')
        engine = create_engine(db_url_string, echo=False)
        session_func = sessionmaker(bind=engine)
        cls.session = session_func()

        # initialization with non nullable values
        cls.entry = Transfer(src_host='myhost.cern.ch',
                             src_path='/data/cdr',
                             file_name='na62raw_1494236504-02-0058-0001.dat',
                             file_size=30,
                             # dst_path is nullable but (see test test_transfer_functions)
                             dst_path='/castor/cern.ch/na62/data/raw/pre2020/009839')
        cls.session.add(cls.entry)
        cls.session.commit()

    @classmethod
    def tearDownClass(cls):
        # If you forgot to close the session alembic will not be able to downgrade
        cls.session.close()
        command.downgrade(cls.alembic_cfg, 'base')

    def test_query_transfer(self):
        results = self.session.query(Transfer).all()
        # assert self.entry in results
        self.assertEqual(results, [self.entry])

    def test_fields_values(self):
        date_now = datetime.utcnow()
        records = self.session.query(Transfer).all()

        self.assertEqual(records[0].file_name, 'na62raw_1494236504-02-0058-0001.dat')
        self.assertEqual(records[0].src_host, 'myhost.cern.ch')
        self.assertEqual(records[0].src_path, '/data/cdr')
        self.assertEqual(records[0].status, 'RECORDED')
        self.assertEqual(records[0].job_id, None)
        self.assertEqual(records[0].file_size, 30)
        self.assertEqual(records[0].is_src_deleted, 0)
        self.assertEqual(records[0].transfer_attempts, 0)
        self.assertEqual(records[0].resubmit_id, None)
        self.assertEqual(records[0].is_invalid, 0)
        self.assertEqual(records[0].is_on_tape, 0)
        self.assertEqual(records[0].comment, None)
        # date fields

        # both updated_at and created_at are evaluated on the db side
        self.assertEqual(records[0].updated_at, records[0].created_at)

        # test updated_at and created_at stored in utc format
        date_now = datetime.utcnow()
        delta = timedelta(minutes=2)
        self.assertGreater(records[0].updated_at, date_now - delta)
        self.assertLess(records[0].updated_at, date_now + delta)

        self.assertGreater(records[0].updated_at, date_now - timedelta(minutes=5))
        self.assertLess(records[0].updated_at, date_now + timedelta(minutes=5))

        self.assertEqual(records[0].transferred_at, None)
        self.assertEqual(records[0].on_tape_at, None)
        self.assertEqual(records[0].deleted_at, None)
        # Default none but set for test
        self.assertEqual(records[0].dst_path, '/castor/cern.ch/na62/data/raw/pre2020/009839')

    def test_transfer_functions(self):
        """
        Test the local and remote full paths
        dst_host is nullable in the db schema but this test fails if is set to null
        """
        self.assertEqual(self.entry.get_source_path(),
                         '/data/cdr/na62raw_1494236504-02-0058-0001.dat')
        self.assertEqual(self.entry.get_destination_path(),
                         '/castor/cern.ch/na62/data/raw/pre2020/009839/'
                         'na62raw_1494236504-02-0058-0001.dat')

def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestTransferModelClass, 'test'))
    return test_suite


if __name__ == '__main__':
    unittest.main()
