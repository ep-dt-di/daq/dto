# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import os
import unittest
from unittest import mock
from datetime import datetime, timedelta
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from alembic import command
from alembic.config import Config

from controllers.DtoController import DtoController
from controllers.SourceController import SourceController
from models.Transfer import Transfer
from tests.helpers.FakeFtsController import FakeFtsController


class TestDtoControllerSubmit(unittest.TestCase):
    alembic_cfg = Config('./ci/alembic-test.ini')
    source_dir = '/root/dto_functional_tests'

    @classmethod
    def setUpClass(cls):
        command.upgrade(cls.alembic_cfg, 'head')

        # reusing the same configuration stored in the alembic file
        db_url_string = cls.alembic_cfg.get_main_option('sqlalchemy.url')
        engine = create_engine(db_url_string, echo=False)
        session_func = sessionmaker(bind=engine)
        cls.session = session_func()

        cls.fts_instance = FakeFtsController('http://myendpoint.ch')
        cls.dto = DtoController(cls.fts_instance, 'myhostname.domain.ch')

        cls.mock_env = mock.patch.dict(os.environ, {
            "regex_group_index": "-1", # regex disabled
            "base_destination": "/test/destination",
            "protocol_base": "https://test.cern.ch/dto/cern.ch",
            "jump_base_destination": "",
            "jump_protocol_base": "",
        })
        cls.mock_env.start()

    @classmethod
    def tearDownClass(cls):
        # If you forgot to close the session alembic will not be able to downgrade
        cls.session.close()
        command.downgrade(cls.alembic_cfg, 'base')
        cls.mock_env.stop()

    def setUp(self):
        old_record = Transfer(id=1,
                              src_path='/root',
                              file_name='file1.dat',
                              file_size=45,
                              src_host='myhostname.domain.ch',
                              updated_at=datetime(2017, 11, 28, 10, 55, 25))
        recent_record = Transfer(id=2,
                                 src_path='/root',
                                 file_name='file2.dat',
                                 file_size=45,
                                 transfer_attempts=5, # Simulating a resubmission
                                 src_host='myhostname.domain.ch',
                                 updated_at=datetime(2020, 1, 4, 12, 40, 10))
        invalid_record = Transfer(id=3,
                                  src_path='/root',
                                  file_name='file2.dat',
                                  file_size=45,
                                  src_host='myhostname.domain.ch',
                                  updated_at=datetime(2020, 1, 5, 16, 10, 35),
                                  is_invalid=1)

        self.session.add_all([old_record, recent_record, invalid_record])
        self.session.commit()

    def tearDown(self):
        # clean up database
        self.session.query(Transfer).delete()
        self.session.commit()


    def test_submit_function(self):
        """
        Test the internal _submit function
        """
        # fetching the record from the db
        records = self.session.query(Transfer).order_by(Transfer.id.asc())
        record_to_submit = records[0]
        source_object = SourceController('myhostname.domain.ch', record_to_submit.get_source_path())

        # Executing the function to test
        (job_id, db_id) = self.dto._submit(self.session, source_object, record_to_submit)

        # fetching again the record from the db
        records = self.session.query(Transfer).order_by(Transfer.id.asc())
        #records = self.session.query(Transfer).order_by(Transfer.id.asc())
        record_updated = records[0]

        # test status updated
        self.assertEqual(record_updated.id, db_id)
        self.assertEqual(record_updated.status, 'SUBMITTED')
        self.assertEqual(record_updated.job_id, job_id)
        self.assertEqual(record_updated.transfer_attempts, 1)
        self.assertEqual(record_updated.src_host, 'myhostname.domain.ch')
        self.assertEqual(record_updated.src_path, '/root')
        self.assertEqual(record_updated.file_name, 'file1.dat')
        self.assertEqual(record_updated.file_size, 45)
        self.assertEqual(record_updated.dst_path, '/test/destination')
        self.assertEqual(record_updated.jump_path, None)


    def test_oldest_updated_first(self):
        date_now = datetime.utcnow()
        delta = timedelta(minutes=2)
        self.dto.new_files_limit = 1 # set new_files_limit to 1

        #########
        # first record
        result = self.dto.submit_files(self.session)
        self.assertEqual(result, (1, 0))

        # fetching the records from the db
        records = self.session.query(Transfer).order_by(Transfer.id.asc())

        # check the old record status
        self.assertEqual(records[0].status, 'SUBMITTED')
        self.assertEqual(records[0].transfer_attempts, 1)
        # test updated_at updated in utc format
        self.assertGreater(records[0].updated_at, date_now - delta)
        self.assertLess(records[0].updated_at, date_now + delta)

        # check the recent record status
        self.assertEqual(records[1].status, 'RECORDED')
        # test updated_at updated in utc format
        self.assertEqual(records[1].updated_at, datetime(2020, 1, 4, 12, 40, 10))

        #########
        # second record
        result = self.dto.submit_files(self.session)
        self.assertEqual(result, (1, 0))

        # fetching the records from the db
        records = self.session.query(Transfer).order_by(Transfer.id.asc())

        # check the old record status
        self.assertEqual(records[0].status, 'SUBMITTED')
        self.assertEqual(records[0].transfer_attempts, 1)
        # test updated_at updated in utc format
        self.assertGreater(records[0].updated_at, date_now - delta)
        self.assertLess(records[0].updated_at, date_now + delta)

        # check the recent record status
        self.assertEqual(records[1].status, 'SUBMITTED')
        self.assertEqual(records[1].transfer_attempts, 6)
        # test updated_at updated in utc format
        self.assertGreater(records[1].updated_at, date_now - delta)
        self.assertLess(records[1].updated_at, date_now + delta)

    def test_invalid_record_ignored(self):
        self.dto.new_files_limit = 10 # Process all record at once
        result = self.dto.submit_files(self.session)
        self.assertEqual(result, (2, 0))

        # fetching the records from the db
        records = self.session.query(Transfer).order_by(Transfer.id.asc())

        #########
        # The third record must not be submitted as it is invalid
        self.assertEqual(records[2].status, 'RECORDED')
        # test updated_at updated in utc format
        self.assertEqual(records[2].updated_at, datetime(2020, 1, 5, 16, 10, 35))

def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestDtoControllerSubmit, 'test'))
    return test_suite


if __name__ == '__main__':
    unittest.main()
