# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import logging
import random
import string

class FakeFtsController:
    """
    Handle the FTS library
    """

    def __init__(self, endpoint, statuses=['FINISHED'], weights=[100]):
        """
        Default result FINISHED 100% of probability
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('Initializing: %s', endpoint)
        self.statuses = statuses
        self.weights = weights

    def submit(self, full_source, full_destination, multi_hop_steps=None,
               metadata=None, overwrite=False):
        """
        Submit a job
        """
        min_char = 36
        max_char = 36
        allchar = string.ascii_lowercase + string.digits
        job_id = "".join(random.choice(allchar) for x in range(random.randint(min_char, max_char)))
        self.logger.debug('Submitting from %s to %s with job_id: %s, metadata: %s, overwrite: %s',
                          full_source, full_destination, job_id, metadata, overwrite)
        return job_id

    def get_status(self, job_id):
        """
        Retrieve the status of a submitted job
        """
        # Python3
        # weighted_random_values = random.choices(self.statuses, weights=self.weights, k=1)
        # status = weighted_random_values[0]

        status = random.choice(self.statuses)

        self.logger.debug('job_id: %s. status: %s', job_id, status)
        return status

    def get_statuses(self, job_ids):
        """
        Retrieve the status of a submitted job
        """
        response = []
        for job_id in job_ids:
            response.append(tuple([job_id, self.get_status(job_id)]))
        return response
