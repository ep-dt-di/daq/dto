import logging.config
import os
import pwd

import fts3.rest.client.easy as fts3

from config import load_configuration_file, validate_configuration


def prepare_environment_for_test():
    # change CRITICAL to ERROR or INFO in order to enable logging during testing
    logging.basicConfig(level=logging.CRITICAL,
                        format='%(asctime)s - %(name)s (%(lineno)s) - %(levelname)s: %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')
    logger = logging.getLogger(__name__)

    if not load_configuration_file():
        return False
    if not validate_configuration():
        return False

    return True


def cancel_all_jobs(fts_instance, host, session=None, model=None, job_ids=None):

    if session and model:
        records = (
            session.query(model)
            .filter_by(src_host=host)
        )

        for record in records:
            try:
                status = fts3.cancel(fts_instance.context, record.job_id)
                # print("Job id: %s, status: %s" % (record.job_id, status["job_state"]))
            except fts3.exceptions.NotFound as ex:
                continue


    if job_ids:

        for job_id in job_ids:
            try:
                status = fts3.cancel(fts_instance.context, job_id)
                # print("Job id: %s, status: %s" % (record.job_id, status["job_state"]))
            except fts3.exceptions.NotFound as ex:
                continue


def set_owner(absolute_filename, owner_name):
    """
    Set the owner to the file
    """
    uid = pwd.getpwnam(owner_name).pw_uid
    gid = os.stat(absolute_filename).st_gid
    return os.chown(absolute_filename, uid, gid)
