# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import logging
import random

class FakeStorageController:
    """Handle Storage (EOS/CTA/GFAL) library"""

    def __init__(self, endpoint, return_values=[True], weights=[100]):
        """
        Default result FINISHED 100% of probability
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug('Initializing: %s', endpoint)
        self.return_values = return_values
        self.weights = weights


    def unlink(self, path):

        value = random.choice(self.return_values)

        self.logger.debug('Remove file: %s. Return value: %s', path, value)
        return value
