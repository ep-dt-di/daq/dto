	import unittest


	class TestExample(unittest.TestCase):

	    @classmethod
	    def setUpClass(cls):
	    	"""This will run before the suite"""
	        pass

	    @classmethod
	    def tearDownClass(cls):
	    	"""This will run after the suite"""
	        pass

	    def setUp(self):
	    	"""This will run before each test"""
	        pass

	    def tearDown(self):
	    	"""This will run after esch test"""
	    	pass

	    def test_example_1(self):
	        pass

	    def test_example_2(self):
	        pass


	def suite():
	    """Create a suite that includes all tests."""
	    test_suite = unittest.TestSuite()
	    test_suite.addTest(unittest.makeSuite(TestExample, 'test'))
	    return test_suite

	if __name__ == '__main__':
    	unittest.main()
