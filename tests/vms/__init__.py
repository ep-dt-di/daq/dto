import unittest

from tests.vms import test_fts_controller

def fsuite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(test_fts_controller.suite())
    return test_suite

def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(fsuite())
    return test_suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite', verbosity=2)
