# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import time
import unittest

import fts3.rest.client.easy as fts3

from controllers.FtsController import FtsController
from controllers.SourceController import SourceController
from tests.helpers import utils

ready_for_test = utils.prepare_environment_for_test()


@unittest.skipIf(ready_for_test is False, "Environment is not ready for testing")
class TestFtsController(unittest.TestCase):
    """Name pattern: test_<name_of_the_tested_function>_<short_description_test_case>"""

    source_dir = u'/root/dto_functional_tests'

    @classmethod
    def setUpClass(cls):
        cls.fts_instance = FtsController(os.environ.get('endpoint'))
        cls.host = os.environ.get('host')
        cls.job_ids = []

    @classmethod
    def tearDownClass(cls):
        # cancel all jobs
        utils.cancel_all_jobs(cls.fts_instance, cls.host, job_ids=cls.job_ids)

    def setUp(self):
        if not os.path.isdir(self.source_dir):
            os.mkdir(self.source_dir)

    def tearDown(self):
        try:
            if os.path.isdir(self.source_dir):
                shutil.rmtree(self.source_dir)
        except Exception as ex:
            pass

    def test_submit_valid_file(self):
        with open(self.source_dir + '/test1.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.host, "%s/test1.txt" % self.source_dir)

        job_id = self.fts_instance.submit(
            source.get_fts_source(),
            source.get_fts_destination(),
            "Test metadata"
        )

        self.assertEqual(len(job_id), 36)

        # add id to cleanup list
        self.job_ids.append(job_id)

        job_status = fts3.get_job_status(self.fts_instance.context, job_id)
        self.assertEqual(job_status['job_metadata'], 'Test metadata')

    def test_submit_no_metadata(self):
        with open(self.source_dir + '/test1.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.host, "%s/test1.txt" % self.source_dir)

        job_id = self.fts_instance.submit(
            source.get_fts_source(),
            source.get_fts_destination()
        )

        self.assertEqual(len(job_id), 36)

        # add id to cleanup list
        self.job_ids.append(job_id)

        job_status = fts3.get_job_status(self.fts_instance.context, job_id)
        self.assertEqual(job_status['job_metadata'], 'Test submission')

    def test_submit_None_source(self):
        with open(self.source_dir + '/test1.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.host, "%s/test1.txt" % self.source_dir)

        with self.assertRaises(fts3.exceptions.ServerError) as context:
            job_id = self.fts_instance.submit(
                None,
                source.get_fts_destination()
            )

    def test_submit_None_destination(self):
        with open(self.source_dir + '/test1.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.host, "%s/test1.txt" % self.source_dir)

        with self.assertRaises(fts3.exceptions.ServerError) as context:
            self.fts_instance.submit(
                source.get_fts_source(),
                None
            )

    def test_submit_inexistent_file(self):
        with open(self.source_dir + '/test1.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.host, "%s/test1.txt" % self.source_dir)

        # remove file
        if os.path.isfile("%s/test1.txt" % self.source_dir):
            os.remove("%s/test1.txt" % self.source_dir)

        job_id = self.fts_instance.submit(
            source.get_fts_source(),
            source.get_fts_destination()
        )

        # add id to cleanup list
        self.job_ids.append(job_id)

        self.assertEqual(len(job_id), 36)

    def test_submit_zero_size_file(self):
        with open(self.source_dir + '/test1.txt', 'w') as afile:
            pass

        source = SourceController(self.host, "%s/test1.txt" % self.source_dir)

        job_id = self.fts_instance.submit(
            source.get_fts_source(),
            source.get_fts_destination()
        )

        # add id to cleanup list
        self.job_ids.append(job_id)

        self.assertEqual(len(job_id), 36)

    def test_get_statuses_valid_job_id(self):
        with open(self.source_dir + '/test1.txt', 'w') as afile:
            afile.write('[DTO testing] This file is used by an automated test!')

        source = SourceController(self.host, "%s/test1.txt" % self.source_dir)

        job_id = self.fts_instance.submit(
            source.get_fts_source(),
            source.get_fts_destination(),
            "Test metadata"
        )

        # add id to cleanup list
        self.job_ids.append(job_id)

        status = self.fts_instance.get_statuses([job_id])
        self.assertEqual(status[0][1], 'SUBMITTED')

    def test_get_statuses_None_job_id(self):
        with self.assertRaises(TypeError) as context:
            self.fts_instance.get_statuses([None])

    def test_get_statuses_empty_string_job_id(self):
        # wait for the cancelation of all jobs
        time.sleep(10)
        status = self.fts_instance.get_statuses([u''])
        self.assertEqual(status, [])

    def test_get_statuses_wrong_format_job_id(self):
        with self.assertRaises(TypeError) as context:
            status = self.fts_instance.get_statuses([1234])

    @unittest.skip("Bug in FTS code: https://its.cern.ch/jira/browse/FTS-1691")
    def test_get_statuses_inexistent_job_id(self):
        with self.assertRaises(fts3.exceptions.NotFound) as context:
            status = self.fts_instance.get_statuses([u'aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee'])


def suite():
    """Create a suite that includes all tests."""
    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestFtsController, 'test'))
    return test_suite


if __name__ == '__main__':
    unittest.main()
