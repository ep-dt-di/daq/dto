# Test documentation
## Testing structure

Tests are grouped in 3 categories:

- 1. unit

In this suite are tests which use [mock](https://pypi.org/project/mock/) library and can be executed in a Docker container

- 2. functional

This suite contains tests which do not need an FTS connection but do not use the mock library. They interact with the OS or DB.  
They should run in a testing enviroment that can be setup in a Docker container

- 3. vms

These tests need a FTS/GFAL connection and can be run only on testing VM.  
The user should config the .env file accordingly.

## Steps for running tests

Running a test suite:

    python3 -m unittest -v tests.unit.suite

OR

    python3 tests/unit/__init__.py

Running a test class:
Export PYTHONPATH:

    export PYTHONPATH=<path_to_dto>:$PYTHONPATH

Example:

    export PYTHONPATH=`pwd`:$PYTHONPATH
    python3 tests/functional/test_dto_controller_get_new_files.py -v TestDtoControllerGetNewFiles

Running a single test:

    python3 tests/functional/test_dto_controller_get_new_files.py -v TestDtoControllerGetNewFiles.test_empty_directory


## Developing and testing locally

Before pushing your changes to any branch you have to execute all the CI test locally.
With the containers help it is possible to reproduce exactly the CI environment.
Before going on, be sure to have podman or docker working on your system.  

Start a local MariaDB instance in detached mode:

    docker run -d --rm --name=mariadb -h mariadb-container \
        -e MYSQL_DATABASE=ci_test \
        -e MYSQL_ROOT_PASSWORD=easy mariadb:10.3

or Mysql5 instance:

    docker run -d --rm --name=mariadb -h mysql-container \
        -e MYSQL_DATABASE=ci_test \
        -e MYSQL_ROOT_PASSWORD=easy mysql:5.7.26 --skip-ssl

or Mysql8 instance:

    docker run -d --rm --name=mariadb -h mysql-container \
        -e MYSQL_DATABASE=ci_test \
        -e MYSQL_ROOT_PASSWORD=easy mysql:8.0.23 --default-authentication-plugin=mysql_native_password

Plese note only a container with the named `mariadb` can exists.  

From the project root directory start a DTO container with the following command:  

    docker run -it --rm --name=dtocc7 -h dto-container \
        --mount type=bind,source="$(pwd)",target=/dto \
        --link mariadb \
        --workdir /dto gitlab-registry.cern.ch/ep-dt-di/daq/dto /bin/bash


The command above will do many things:
- link the DTO container to the database that you previously launched
- expose your local directory in to the container (any change outside is propagated inside and vice-versa)
- open a shell in a DTO environment

You can finally run tests:

    python3 -m unittest -v tests.unit.suite
    python3 -m unittest -v tests.functional.suite

As well pylint:

    pylint-3 controllers/*.py models/*.py *.py --output-format=colorized --rcfile=ci/pylintrc
    pylint-3 tests/functional/*.py tests/unit/*.py tests/vms/*.py tests/helpers/*.py --output-format=colorized --rcfile=ci/pylintrc


### Python3 tests
You can also execute all the tests in the CI Python 3 environment, to do that you need to use the CentOS8 Python 3 image:


    docker run -it --rm --name=dtoc8 -h dto-container3 \
        --mount type=bind,source="$(pwd)",target=/dto \
        --workdir /dto --link mariadb gitlab-registry.cern.ch/ep-dt-di/daq/dto/c8 /bin/bash

For the tests:

    python3 -m unittest -v tests.unit.suite
    python3 -m unittest -v tests.functional.suite

and pylint:

    pylint-3 controllers/*.py models/*.py *.py --output-format=colorized --rcfile=ci/pylintrc
    pylint-3 tests/functional/*.py tests/unit/*.py tests/vms/*.py tests/helpers/*.py --output-format=colorized --rcfile=ci/pylintrc
