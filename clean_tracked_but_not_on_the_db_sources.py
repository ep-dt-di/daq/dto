#!/usr/bin/env python3
#
# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

# Checks for untracked files in the directory
import os
import sys
import glob

from config import load_configuration_file, validate_configuration, get_list_from_string
from controllers.SourceController import SourceController
from models.Transfer import Transfer
from models.utils import get_db_session

if __name__ == '__main__':
    if not load_configuration_file():
        sys.exit(1)
    if not validate_configuration():
        sys.exit(1)

    session = get_db_session(os.environ.get('db_user'),
                             os.environ.get('db_pass'),
                             os.environ.get('db_host'),
                             os.environ.get('db_port'),
                             os.environ.get('db_name')
                            )()
    host = os.environ.get('host')

    file_dirs = get_list_from_string(os.environ.get('file_dirs'))
    #Using the first directory
    file_dir = file_dirs[0]

    amount = (
        session.query(Transfer)
        .filter_by(is_invalid=0)
        .filter_by(status='FINISHED')
        .filter_by(is_src_deleted=0)
        .filter_by(src_host=host)
        .order_by(None).count()
    )

    files = list(filter(os.path.isfile, glob.glob(file_dir + "/*")))
    source_amount = len(files)
    if amount < source_amount:
        files = sorted(files, key=lambda x: os.path.getmtime(x))

        # print amount
        # print source_amount
        print("There are: ", (source_amount - amount), " untracked files")

        counter = 1
        offset = 0
        limit = 70
        amount_of_result = limit
        # I'm breaking the query in order to minimize the database load
        while amount_of_result == limit:
            # print "Iteration number: " , counter
            records = session. \
                query(Transfer). \
                filter_by(is_invalid=0). \
                filter_by(status='FINISHED'). \
                filter_by(is_src_deleted=0). \
                filter_by(src_host=host). \
                order_by(Transfer.id.asc()). \
                slice(offset, limit). \
                offset(offset). \
                limit(limit). \
                all()

            # amount_of_result = records.count()
            amount_of_result = 0
            for record in records:
                amount_of_result += 1
                source_path = record.get_source_path()
                if source_path in files:
                    try:
                        # removing db entry from the os list
                        files.remove(source_path)
                    except Exception as ex:
                        print("Entry doesn't exist. Exception message: " + str(ex))

            # print "New list len: " , len(files)
            counter += 1
            offset += limit

        print("List len: ", len(files))
        print(len(files), " Untracked files:")
        for my_file in files:
            new_source = SourceController(host, my_file)


            if new_source.get_owner() == os.environ.get(
                    'source_owner') and new_source.get_group() == os.environ.get('source_group'):
                print(my_file + " Was tracked")
                new_source = SourceController(host, my_file)
                print('deleting')
                new_source.delete()
                # new_source.rename()
            else:
                print(my_file + " Was not tracked")
                continue
    else:
        print("Nothing untracked")
