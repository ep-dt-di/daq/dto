#!/usr/bin/env python3
#
# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import sys
from datetime import datetime, timedelta

from config import load_configuration_file, validate_configuration
from controllers.CtaController import CtaController
from controllers.SourceController import SourceController
from models.Transfer import Transfer
from models.utils import get_db_session

if __name__ == '__main__':

    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    logger = logging.getLogger(__name__)
    if not load_configuration_file():
        sys.exit(1)
    if not validate_configuration():
        sys.exit(1)

    session = get_db_session(os.environ.get('db_user'),
                             os.environ.get('db_pass'),
                             os.environ.get('db_host'),
                             os.environ.get('db_port'),
                             os.environ.get('db_name')
                            )()

    host = os.environ.get('host')
    minutes = 60 * 6
    allow_resubmit_before = datetime.utcnow() + timedelta(minutes=-int(minutes))
    logger.info('Running on: %s', host)

    records = (
        session.query(Transfer)
        .filter_by(is_invalid=0)
        .filter_by(status='SUBMITTED')
        .filter_by(resubmit_id=None)
        .filter_by(src_host=host)
        .filter(Transfer.updated_at < allow_resubmit_before)
        .order_by(Transfer.transfer_attempts.desc())
        .limit(10)
    )

    resubmit_count = 0
    suitable_for_resubmission_count = 0

    if records.count() > 0:
        castor = CtaController()
        for record in records:
            source_path = record.src_path + record.file_name
            source = SourceController(host, source_path)
            local_exist = source.exists()
            remote_exist = castor.file_exists(record.get_destination_path())
            if not remote_exist and local_exist:
                suitable_for_resubmission_count += 1

            # if remote_exist:
            #    print "Exist"
            ##    if remote_size == 0:
            ##        size_zero_count += 1
            ##        if database_size == local_size:
            ##            suitable_for_overwrite_count += 1
            ##    else:
            ##        size_non_zero_count += 1
            ##        if database_size == local_size and database_size == local_size:
            ##            fail_fetch_status_count += 1
            ##        else:
            ##            file_recreated_count += 1
            # else:
            #    print "Dont Exist"
            #    remote_dont_exist += 1

            logger.info(str(record.id) + " " + str(record.job_id) + " " +
                        str(record.transfer_attempts) + " Local File Exixst: " + str(local_exist) +
                        " Remote File Exist: " + str(remote_exist) +
                        " " + record.get_source_path() + " " + record.get_destination_path()
                       )

        logger.info('Found: %s Records', records.count())
        logger.info("Cancelled file suitable for resubmission: %s", suitable_for_resubmission_count)
    else:
        logger.info('No records found')
