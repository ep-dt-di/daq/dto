# Globus-gridftp-server installation and configuration
Get root privileges:

    sudo su

## Certificates
### Generate the user certificate
Request a user certificate at https://ca.cern.ch.  
A user certificate can be used on many hosts.  
Copy the certificate in the `/etc/robot-certificate` directory on the remote machine.  
Convert the `.p12` certificate to `.pem`:

    cd /etc/robot-certificate
    openssl pkcs12 -in usercertificate.p12 -nocerts -nodes > userkey.pem
    openssl pkcs12 -in usercertificate.p12 -nokeys -nodes > usercert.pem
    chmod 0600 user*

### Generate the host certificate
Request a host certificate at https://ca.cern.ch.  
Each host needs his own certificate.  
Copy the host certificate in the `/etc/grid-security` directory on the remote machine.  
Convert the `.p12` certificate to `.pem`:

    cd /etc/grid-security
    openssl pkcs12 -in hostcertificate.p12 -nocerts -nodes > hostkey.pem
    openssl pkcs12 -in hostcertificate.p12 -nokeys -nodes > hostcert.pem
    chmod 0600 host*


## Install the storage element

    yum install -y m2crypto python-m2ext voms-clients-cpp globus-gridftp-server edg-mkgridmap globus-gridftp-server-progs

### Open firewall ports
Open the following ports:
- 2811/tcp port for the grid
- 20000-21000/tcp Open ports for GLOBUS_TCP_PORT_RANGE

Execute the following commands:

    firewall-cmd --zone=public --permanent --add-port=2811/tcp
    firewall-cmd --zone=public --permanent --add-port=20000-21000/tcp
    firewall-cmd --reload

Check the updated rules with:

    firewall-cmd --list-all

Without the "--permanent" flag, the firewall rule would not persist across reboots.


### Configure gridmap
Create the `/etc/edg-mkgridmap.conf` file

    vim /etc/edg-mkgridmap.conf

Paste in the following configuration:

    ##############################################################################
    # EDG Virtual Organisations
    # eg 'group ldap://grid-vo.cnaf.infn.it/ou=testbed1,o=infn,c=it .infngrid'
    ##############################################################################


    group "vomss://voms.gridpp.ac.uk:8443/voms/na62.vo.gridpp.ac.uk" na62cdr

    #############################################################################
    # List of auth URIs
    # eg 'auth ldap://marianne.in2p3.fr/ou=People,o=testbed,dc=eu-datagrid,dc=org'
    #
    # If these are defined then users must be authorised in one of the following
    # auth servers.
    #############################################################################

    #############################################################################
    # DEFAULT_LCLUSER: default_lcluser lcluser
    # e.g. 'default_lcuser .'
    #############################################################################

    #############################################################################
    # ALLOW and DENY: deny|allow pattern_to_match
    # e.g. 'allow *INFN*'
    #############################################################################

    #############################################################################
    # Local grid-mapfile to import and overide all the above information.
    # e.g. 'gmf_local /etc/localgridmap.conf'
    #############################################################################

    gmf_local /etc/localgridmap.conf

replace the name na62cdr with the username

Create the `/etc/localgridmap.conf` file.

Pull the certificate known by the VO and push loca mapping with the following command

    edg-mkgridmap


## Set up automatic gridmap polling with a cron

    crontab -e

Paste in:

    17 */2 * * * /usr/sbin/edg-mkgridmap --conf=/etc/edg-mkgridmap.conf --output=/etc/grid-security/grid-mapfile --safe --cache —quiet

## Configure the vomses

    mkdir /etc/vomses
    cd /etc/vomses

Create:

    vim na62.vo.gridpp.ac.uk-voms02.gridpp.ac.uk

copy inside:

    "na62.vo.gridpp.ac.uk" "voms02.gridpp.ac.uk" "15501" "/C=UK/O=eScience/OU=Oxford/L=OeSC/CN=voms02.gridpp.ac.uk" "na62.vo.gridpp.ac.uk" "24"

Create:

    vim na62.vo.gridpp.ac.uk-voms03.gridpp.ac.uk

copy inside:

    "na62.vo.gridpp.ac.uk" "voms03.gridpp.ac.uk" "15501" "/C=UK/O=eScience/OU=Imperial/L=Physics/CN=voms03.gridpp.ac.uk" "na62.vo.gridpp.ac.uk" "24"

Create:

    vim na62.vo.gridpp.ac.uk-voms.gridpp.ac.uk

copy inside:

    "na62.vo.gridpp.ac.uk" "voms.gridpp.ac.uk" "15501" "/C=UK/O=eScience/OU=Manchester/L=HEP/CN=voms.gridpp.ac.uk" "na62.vo.gridpp.ac.uk" "24"


### Connection issues
Please notice that edg-mkgridmap will fail if there are network restictions and the gridmap has to be copied over statically

    [how to set up a static gridmap: TODO]

## Start the storage element

    systemclt start globus-gridftp-server
    systemclt status globus-gridftp-server

## Init the proxy
Generates the temporary proxy:

    voms-proxy-init --cert usercert.pem --key userkey.pem

Generates the temporary proxy with voms and hours of lifetime:

    voms-proxy-init --cert ./na62cdrdmcert.pem --key ./na62cdrdmkey.pem --voms na62.vo.gridpp.ac.uk -hours 36

## Set up automatic proxy renewal with cron

    crontab -e

Paste in:

    0 */4 * * * voms-proxy-init --cert /etc/robot-certificate/na62cdrcert.pem --key /etc/robot-certificate/na62cdrkey.pem -hours 24 -q

## Useful links

- https://gitlab.cern.ch/ep-dt-di/daq/dto.git

