# FTS CLI examples
You need first to configure the globus-grid-ftp-server and install the the FTS Python client:
- install and configure [globus gridftp server](doc/install-globus-gridftp-server.md)
- install [FTS python client](doc/install-fts-python-client.md)

## Get the proxy certificate
Get your certificate without the proxy

    fts-rest-whoami -s https://fts3-daq.cern.ch:8446 --cert=./usercert.pem --key=./userkey.pem -vv


Get your certificate with the proxy

    fts-rest-whoami -s https://fts3-daq.cern.ch:8446

## Transfer a file
Submit a transfer from CASTOR to dpm

    fts-rest-transfer-submit -s https://fts3-daq.cern.ch:8446 srm://srm-public.cern.ch:8443/srm/managerv2?SFN=/castor/cern.ch/na62/data/2017/pippo.txt https://dpmhead-rc.cern.ch/dpm/cern.ch/home/dteam/aalvarez/public/pippo.txt

Transfer from a host to CASTOR

    fts-rest-transfer-submit -s https://fts3-daq.cern.ch:8446 -o gsiftp://na62merger5.cern.ch/storage/1.data srm://srm-public.cern.ch:8443/srm/managerv2?SFN=/castor/cern.ch/na62/data/2017/pippo.txt

Transfer from a host to EOS

    fts-rest-transfer-submit -s https://fts3-daq.cern.ch:8446 gsiftp://na62merger1.cern.ch/merger/1.data root://eospublic.cern.ch//eos/experiment/na62/user/n/na62cdr/1.data


## Check the transfer status

    fts-rest-transfer-status -s https://fts3-daq.cern.ch:8446 04ba6736-1f87-11e7-8d47-02163e007a99A

## Monitoring

https://fts3-daq.cern.ch:8449

https://monit.cern.ch

