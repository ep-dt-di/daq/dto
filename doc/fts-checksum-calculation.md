# Files checksum calculation
FTS use the `xrdadler32` algorithm to compute the checksum of a files.  
The command `xrdadler32` is part of the xrootd-client package, you can in install it from EPEL.

	yum install -y xrootd-client

Here's how you can evaluate the checksum on a submitting host:

	xrdadler32  /merger/dto/na62raw_1524108472-01-008511-0891.dat

Output:

	fbbf5cd5 /merger/dto/na62raw_1524108472-01-008511-0891.dat
