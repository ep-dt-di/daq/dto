# Install gfal2 Python3 client on CentOS7

## Epel repo installation (recommended)

    yum install -y epel-release
    yum install -y gfal2-python3

## Dmc repo installation

    yum-config-manager --add-repo http://dmc-repo.web.cern.ch/dmc-repo/dmc-el7.repo
    yum install -y gfal2-python3

