# DTO installation
Instructions for CentOS7 and Python3.

- install and configure [globus gridftp server](doc/install-globus-gridftp-server.md)
- install [gfal2 python client](doc/install-gfal2-python-client.md)
- install [FTS python client](doc/install-fts-python-client.md)
- install and configure Mariadb Database

## Install DTO dependencies

        yum install -y python3-pip python36-pylint python36-mysql python36-sqlalchemy python36-mock
        pip3 --no-cache-dir install alembic python-dotenv

## Create the book-keeping table

	alembic upgrade head


## Create the configuration file
To get started copy an example `.env` file from the `doc/configuration-examples/` in the project base directory:

    cp doc/configuration-examples/.testing_env .env

### Additional logging configuration
When the system is in production is recommended to add an additional configuration file for the logging messages.
An example of the `dto-logging.ini` can be found in `doc/configuration-examples/`.

    sudo mkdir -p /etc/dto.d
    sudo cp doc/configuration-examples/dto-logging.ini /etc/dto.d/dto-logging.ini

