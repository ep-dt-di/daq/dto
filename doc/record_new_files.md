# Record new files

# default behaviour
Only when a file is ready have to be recorded and then transferred. 
As a default behaviour DTO records all the files that belong to a certain user (see source_owner paramenter).
The same file should not be recorded twice, for this reason the group owner is changed after a correct recording (see source_owner and source_group parameter)

Example: 
-rw-rw-rw-. 1 root root 795 16 déc.  14:03 data1.dat <- DAQ process is writing the file
-rw-rw-rw-. 1 marco root 795 16 déc.  14:03 data1.dat <- File completed by the DAQ, ownership marco (this file will be returned by the _get_new_files function)
-rw-rw-rw-. 1 marco marco 795 16 déc.  14:03 data1.dat <- File was recoded by the _mark_as_recorded function and the group now is marco. The file will not be listed anymore by the _get_new_files function.


# Custom behaviour

If you want to change the default behaviour you have to overwrite the following functions:
def _get_new_files(self, source_dir):
def _mark_as_recorded(self, recorded_file):

The first looks for new files in a directory and returns the files to transfer. The second marks the file as recorded in this way is not listed anymore between the files to transfer by the first function

