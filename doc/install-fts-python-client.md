# Install FTS Python3 client on CentOS7
The FTS client can be installed in 3 ways

## FTS prod repo installation (recommeded)


    yum-config-manager --add-repo https://fts-repo.web.cern.ch/fts-repo/fts3-prod-el7.repo
    yum install -y fts-rest-client-3.12.0 --enablerepo=fts3-prod-el7


## Pip installation
https://pypi.org/project/fts3/

    pip install fts3
