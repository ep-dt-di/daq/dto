# File routing
The destination directory can be calculated from the file name. This allow to organise the files in direcories (by date, or data-taking conditions). This is colled "routing".

In order to do that you have to provide a regex to match the file name (regex parameter) and specify the group matched by the regex (regex_group_index parameter).

regex_group_index set to -1 means routing not enabled.

