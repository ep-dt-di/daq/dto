#!/usr/bin/env python3
#
# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

# check for submited file with size 0
import os
import sys
from config import load_configuration_file, validate_configuration
from controllers.SourceController import SourceController
from models.Transfer import Transfer
from models.utils import get_db_session

if __name__ == '__main__':
    if not load_configuration_file():
        sys.exit(1)
    if not validate_configuration():
        sys.exit(1)

    session = get_db_session(os.environ.get('db_user'),
                             os.environ.get('db_pass'),
                             os.environ.get('db_host'),
                             os.environ.get('db_port'),
                             os.environ.get('db_name')
                            )()

    host = os.environ.get('host')

    print("Running on: " + host)
    records = (
        session.query(Transfer)
        .filter_by(is_invalid=0)
        .filter_by(status='FAILED')
        .filter_by(resubmit_id=None)
        .filter_by(src_host=host)
        .filter_by(file_size=0)
        .limit(10)
    )
    # print str(records)

    if records.count() > 0:
        for record in records:
            new_source = SourceController(record.src_host, record.get_source_path())
            print(record)
            print("Source File size: " + str(record.file_size) + " " + record.get_source_path())
    else:
        print("No records found")
