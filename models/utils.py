# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

def get_db_session(database_user,
                   database_password,
                   database_host,
                   database_port,
                   database_name
                  ):
    """
    Produce a new Session object using the configuration established in this sessionmaker

    Args:
      database_user (str)
      database_password (str)
      database_host (str)
      database_port (str)
      database_name (str)

    Returns:
      Session object (sqlalchemy.orm.session.Session)
    """
    engine = create_engine(
        'mysql://' + database_user +
        ':' + database_password +
        '@' + database_host +
        ':' + database_port +
        '/' + database_name,
        echo=False
    )
    return sessionmaker(bind=engine)
