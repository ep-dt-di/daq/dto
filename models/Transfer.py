# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import Column, Integer, String, Enum, ForeignKey, TIMESTAMP
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func

class Transfer(declarative_base()):
    """
    Model for the table: transfers
    """
    __tablename__ = 'transfers'

    id = Column(Integer, primary_key=True)
    file_name = Column(String(255), nullable=False)
    src_host = Column(String(255), nullable=False)
    src_path = Column(String(255), nullable=False)
    dst_path = Column(String(255))
    status = Column(
        Enum(
            'RECORDED',
            'SUBMITTED',
            'ACTIVE',
            'FINISHED',
            'READY',
            'STAGING',
            'STARTED',
            'FAILED'
        ),
        default='RECORDED'
    )
    job_id = Column(String(255), nullable=True, default=None)
    # Interesting explanation of server_default, default, onupdate, server_onupdate:
    # https://stackoverflow.com/questions/13370317/sqlalchemy-default-datetime
    created_at = Column(TIMESTAMP(timezone=True), server_default=func.utcnow())
    updated_at = Column(TIMESTAMP(timezone=True), nullable=False,
                        server_default=func.utcnow(), server_onupdate=func.utcnow())
    transferred_at = Column(TIMESTAMP(timezone=True), nullable=True, default=None)
    on_tape_at = Column(TIMESTAMP(timezone=True), nullable=True, default=None)
    file_size = Column(Integer, nullable=False)
    is_src_deleted = Column(Integer, default=0)
    deleted_at = Column(TIMESTAMP(timezone=True), nullable=True, default=None)
    transfer_attempts = Column(Integer, default=0)
    resubmit_id = Column(Integer, ForeignKey('transfers.id'))
    is_invalid = Column(Integer, default=0)
    is_on_tape = Column(Integer, default=0)
    comment = Column(Integer, default=None)
    jump_path = Column(String(255), nullable=True, default=None)
    is_jump_file_deleted = Column(Integer, default=0)
    jump_file_deleted_at = Column(TIMESTAMP(timezone=True), nullable=True, default=None)

    def __repr__(self):
        return "<Transfer(%s %s %s %s %s %s %s %s %s %s %s %s)>" % (
            self.id, self.job_id, self.src_host, self.status, self.src_path, self.file_name,
            self.dst_path, self.created_at,
            self.updated_at, self.deleted_at,
            self.transfer_attempts, self.jump_path)

    def get_source_path(self):
        """
        Return the full source path: src_path + file_name
        """
        return self.src_path + '/' + self.file_name

    def get_destination_path(self):
        """
        Return the full destination path: dst_path + file_name
        """
        return self.dst_path + '/' + self.file_name

    def get_jump_path(self):
        """
        Return the full jump path: dst_path + file_name
        """
        return str(self.jump_path) + '/' + self.file_name
