# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import os
import logging
import pwd
import grp
import re

class SourceController:
    """
    Handle source and destination of a local file
    """

    def __init__(self, host, source):
        self.logger = logging.getLogger(__name__)
        self.host = host
        self.source = source

    def get_source(self):
        """
        Return the source full name
        """
        return self.source

    def get_owner(self):
        """
        Return the Source owner
        """
        try:
            return pwd.getpwuid(os.stat(self.source).st_uid).pw_name
        except (KeyError, TypeError, OSError) as ex:
            self.logger.error("Cannot fetch the file owner %s", ex)
            return 'nobody'

    def get_group(self):
        """
        Return the group owner
        """
        try:
            group_id = os.stat(self.source).st_gid
            return grp.getgrgid(group_id)[0]
        except (KeyError, TypeError, OSError) as ex:
            # Cannot find the group
            self.logger.error("Cannot fetch the file group %s", ex)
            return 'nogroup'

    def set_group(self, group_name):
        """
        Set the group to the file
        """
        try:
            uid = os.stat(self.source).st_uid
            gid = grp.getgrnam(group_name).gr_gid
            return os.chown(self.source, uid, gid)
        except (TypeError, KeyError) as ex:
            self.logger.error("Invalid group_name: %s. Error message: %s", str(group_name), str(ex))
            raise
        except OSError as ex:
            self.logger.error("Cannot fetch the file owner.Error: %s", ex)
            raise

    def get_dir_name(self):
        """
        Return the file path
        """
        return os.path.dirname(self.source)

    def get_base_name(self):
        """
        Return the Source basename
        """
        return os.path.basename(self.source)

    def get_size(self):
        """
        Return the Source size
        """
        return os.path.getsize(self.source)

    def is_size_zero(self):
        """
        Return True if the Source file is 0
        """
        return self.get_size() == 0

    def delete(self):
        """
        Delete the Source
        """
        if self.exists():
            os.remove(self.source)
            return True

        return False

    def rename(self):
        """
        Rename the source appending .del at the end
        """
        if self.exists():
            os.rename(self.source, self.source + '.del')
            return True

        return False

    def exists(self):
        """
        Return True if the file exist locally
        """
        return os.path.exists(self.source)

    def get_destination(self):
        """
        Return the file destination
        """
        return self.route_with_regex(os.environ.get('base_destination'),
                                     os.environ.get('regex'),
                                     int(os.environ.get('regex_group_index'))
                                    )

    def route_with_regex(self, base_destination, regex, regex_group_index):
        """
        Return the file destination evaluated with regex function
        """
        destination = base_destination
        # Check if the regex is disabled
        if regex_group_index < 0:
            return destination

        regex_match = re.match(
            regex,
            self.get_base_name()
        )

        if regex_match:
            destination += '/' + regex_match.group(regex_group_index)
        else:
            # Regex has not been recognised pushing in the home dir
            self.logger.error("The regex to find the file destination failed file %s",
                              self.get_base_name())
        return destination

    def get_fts_source(self):
        """
        Return the FTS link
        """
        return 'gsiftp://' + self.host + self.source

    def get_fts_destination(self):
        """
        Build the full destination link:
        base (including protocol) + destination server path + filename
        Example base:
        srm (for CASTOR): srm://srm-public.cern.ch:8443/srm/managerv2?SFN=
        https (for testing): https://dpmhead-rc.cern.ch/dpm/cern.ch
        etc
        """
        return os.environ.get('protocol_base') + \
               str(self.get_destination()) + '/' + self.get_base_name()

    def get_jump_destination(self):

        jump_base_destination = os.environ.get('jump_base_destination')

        if jump_base_destination != "":
            routing_path = self.route_with_regex(jump_base_destination,
                                                 os.environ.get('regex'),
                                                 int(os.environ.get('regex_group_index')))
            return routing_path

        return None

    def get_jump_full_destination(self):

        if (os.environ.get('jump_protocol_base') != "" and
                os.environ.get('jump_base_destination') != ""):
            return os.environ.get('jump_protocol_base') + \
               str(self.get_jump_destination()) + '/' + self.get_base_name()
        return None
