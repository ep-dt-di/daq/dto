# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import logging
import signal

class GracefulInterruptController:
    """
    Capture signals
    https://stackoverflow.com/questions/1112343/how-do-i-capture-sigint-in-python
    """

    def __init__(self, sig=signal.SIGINT):
        self.logger = logging.getLogger(__name__)
        self.sig = sig
        self.interrupted = False
        self.released = False
        self.original_handler = None

    def __enter__(self):
        self.interrupted = False
        self.released = False
        self.original_handler = signal.getsignal(self.sig)

        def handler(signum, frame):
            self.release()
            self.interrupted = True

        signal.signal(self.sig, handler)
        self.logger.debug('Graceful interrupt controller is listening')
        return self

    def __exit__(self, ext_type, ext_value, ext_tb):
        self.release()

    def release(self):
        self.logger.debug('Graceful interrupt controller is releasing')
        if self.released:
            return False

        signal.signal(self.sig, self.original_handler)
        self.released = True
        return True
