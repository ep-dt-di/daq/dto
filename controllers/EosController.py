# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

from controllers.StorageController import StorageController

class EosController(StorageController):
    """
    Allow to check the file status using the gfal2 library
    """

    def __init__(self, endpoint='root://eospublic.cern.ch/'):
        super(EosController, self).__init__(endpoint)
        #Python 3:
        #super().__init__(endpoint)
