# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import logging
from datetime import timedelta

import fts3.rest.client.easy as fts3

class StatusUnknown(Exception):
    """Raised when the job status is unknown"""

class FtsController:
    """
    Handle the FTS library
    """

    def __init__(self, endpoint):
        self.logger = logging.getLogger(__name__)
        self.endpoint = endpoint
        self.context = fts3.Context(self.endpoint)

    def submit(self, full_source, full_destination, multi_hop_steps=None,
               metadata=None, overwrite=False):
        """
        Submit a job
        """
        self.context._set_x509(None, None)
        checksum = None  # if you already have a checksum of the file
        if metadata is None:
            metadata = {}

        if multi_hop_steps is None:
            multi_hop_steps = []

        multi_hop_steps.insert(0, full_source)
        multi_hop_steps.append(full_destination)

        number_of_transfers = len(multi_hop_steps) - 1

        is_multi_hop = False
        if number_of_transfers > 1:
            is_multi_hop = True

        transfers = []

        try:
            for index in range(number_of_transfers):
                transfer = fts3.new_transfer(
                    multi_hop_steps[index], multi_hop_steps[index + 1],
                    checksum=checksum, filesize=None,
                    metadata=metadata)
                transfers.append(transfer)

            # retry 1 will not retry the transfer
            job = fts3.new_job(
                transfers,
                verify_checksum=True,
                overwrite=overwrite,
                metadata=metadata,
                retry=3,
                multihop=is_multi_hop
            )
            job_id = fts3.submit(self.context, job, timedelta(hours=7), force_delegation=True)

            self.logger.debug('Submitted: %s', job_id)
            return job_id
        except fts3.exceptions.TryAgain as ex:
            self.logger.error('Cannot submit: %s. %s', full_source, ex)
            raise
        except fts3.exceptions.ServerError as ex:
            self.logger.error('Server error. Cannot submit: %s. %s', full_source, ex)
            raise
        except Exception as ex:
            self.logger.error('Unexpected error submitting: %s. %s', full_source, ex)
            raise

    def get_status(self, job_id):
        """
        Retrieve the status of a submitted job
        """
        try:
            job_status = fts3.get_job_status(self.context, job_id)
        except fts3.exceptions.TryAgain as ex:
            self.logger.error('Error while retrieving status of job_id: %s. %s', job_id, ex)
            raise
        except fts3.exceptions.Unauthorized as ex:
            self.logger.error("Unauthorized, wrong credentials which certificate are you using?")
            raise
        except fts3.exceptions.NotFound as ex:
            self.logger.error('Error while retrieving status of job_id: %s. %s', job_id, ex)
            raise StatusUnknown
        except Exception as ex:
            self.logger.error('Error while retrieving status of job_id: %s. %s', job_id, ex)
            raise

        self.logger.debug('job_id: %s status: %s', job_id, job_status['job_state'])
        return tuple([job_id, job_status['job_state']])

    def get_statuses(self, job_ids):
        """
        Retrieve the status of a submitted job
        """
        try:
            job_statuses = fts3.get_jobs_statuses(self.context, job_ids)
        except fts3.exceptions.TryAgain as ex:
            self.logger.error('Error while retrieving status of job_id: %s. %s', job_ids, ex)
            raise
        except fts3.exceptions.NotFound as ex:
            self.logger.error('Error while retrieving status of job_id: %s. %s', job_ids, ex)
            raise
        except Exception as ex:
            self.logger.error('Error while retrieving status of job_id: %s. %s', job_ids, ex)
            raise

        if isinstance(job_statuses, dict):
            return [(job_statuses['job_id'], job_statuses['job_state'])]

        return [tuple([status['job_id'], status['job_state']]) for status in job_statuses]
