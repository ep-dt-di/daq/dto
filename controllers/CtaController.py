# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import gfal2
from controllers.StorageController import StorageController

class CtaController(StorageController):
    """
    Allow to check the file status using the gfal2 library
    """

    def __init__(self, endpoint='root://eosctapublic.cern.ch/'):
        super().__init__(endpoint)

    def is_on_tape(self, path):
        """
        Return true if a file is on tape
        Others possible value to query
        print context.listxattr(path)
        ['user.replicas', 'user.status', 'srm.type', 'spacetoken']
        """
        if not self.file_exists(path):
            return False

        try:
            castor_status = self.context.getxattr(self.get_complete_url(path), 'user.status')
            self.logger.debug('Castor file status: ' + castor_status + ' ' + path)

            if castor_status.strip() in ["NEARLINE", "ONLINE_AND_NEARLINE"]:
                self.logger.debug('File on tape: %s', path)
                return True

            self.logger.debug('File NOT on tape: %s', path)
            return False
        except gfal2.GError as error:
            message = self.error_template.format(type(error).__name__, error.message)
            self.logger.error('Must be an existing file %s %s. %s', path, str(error.code), message)
            return False
