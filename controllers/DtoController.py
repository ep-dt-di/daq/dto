# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import glob
import logging
import os
import time
from datetime import datetime, timedelta

from controllers.CtaController import CtaController
from controllers.EosController import EosController
from controllers.SourceController import SourceController
from controllers.FtsController import StatusUnknown
from models.Transfer import Transfer

class DtoController:
    """
    Handle all the DTO operations
    """
    def __init__(self, fts_controller, host, eos_storage=None):
        self.logger = logging.getLogger(__name__)
        self.logger.info('Creating an instance of DtoController')
        self.fts_instance = fts_controller
        self.host = host
        self.cta = CtaController()
        if eos_storage is None:
            self.eos_storage = EosController()
        else:
            self.eos_storage = eos_storage
        try:
            self.fetch_limit = int(os.environ.get('fetch_limit'))
        except TypeError as ex:
            self.fetch_limit = 10
        try:
            self.new_files_limit = int(os.environ.get('new_files_limit'))
        except TypeError as ex:
            self.new_files_limit = 10
        try:
            self.limit_resubmission = int(os.environ.get('limit_resubmission'))
        except TypeError as ex:
            self.limit_resubmission = 5

    def _get_new_files(self, source_dir):
        """
        Return a list of new files found in the source dir
        Files with size 0 will be deleted

        Function supposed to be used internally (private)
        You might overwrite this method to change the behaviour

        Args:
          source_dir (str): The absolute path of the directory that contains the files to transfer

        Returns:
          list (:obj:`list` of :obj:`controllers.SourceController`): the list of new file
        """
        amount = 0
        source_dir = source_dir + '/'
        files = filter(os.path.isfile, glob.glob(source_dir + "*"))
        files = sorted(files, key=lambda x: os.path.getmtime(x))
        new_sources = []

        for new_file in files:
            new_source = SourceController(self.host, new_file)

            if new_source.get_owner() == os.environ.get(
                    'source_owner') and new_source.get_group() == os.environ.get('source_group'):
                amount += 1

                if new_source.is_size_zero():
                    self.logger.warning('%s deleted because file size is 0', new_file)
                    new_source.delete()
                    continue

                new_sources.append(new_source)

                if amount >= self.new_files_limit:
                    break

        return new_sources

    def _mark_as_recorded(self, recorded_file):
        """
        Mark a file as recorded,
        it will not be listed by _get_new_files anymore

        Function supposed to be used internally (private)
        You might overwrite this method to change the behaviour

        Todo:
          - catch the specific exception raised

        Args:
          recorded_file (:obj:`list` of :obj:`controllers.SourceController`): the file objet to mark

        Returns:

        Raises:

        """
        recorded_file.set_group(os.environ.get('new_source_group'))

    def _do_record(self, session, new_source, transfer_attempts=0):
        """
        Record a new entry in the database

        Function supposed to be used internally (private)

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object
          new_source (:obj:`controllers.SourceController`): The object of the file that have to be
            stored in the database
          transfer_attempts (int, optional): Transfer attempts of a file
                                             Default = 0
                                             > 0 on resubmission

        Returns:
          (:obj:`models.Transfer`): The database entry associated to the file recorded

        Raises:

        """
        try:
            new_transfer = Transfer(
                src_host=self.host,
                src_path=new_source.get_dir_name(),
                file_name=new_source.get_base_name(),
                file_size=new_source.get_size(),
                transfer_attempts=transfer_attempts)
            session.add(new_transfer)
            session.flush()
            session.commit()
            session.refresh(new_transfer)
        except Exception as ex:
            session.rollback()
            self.logger.error('Cannot add the file %s to the database. %s',
                              new_source.get_source(), str(ex))
            raise

        return new_transfer

    def record_files(self, session, source_dir):
        """
        Record all the new entry found in a directory

        Todo:
          - If _mark_as_recorded raise rollback session

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object
          source_dir (str): The absolute path of the directory that contains the files to transfer

        Returns:
          A tuple of int representing the number of new entry saved in the db and the number of
              entry not successfully saved in the db
        """
        new_sources = self._get_new_files(source_dir)
        record_success = 0

        for new_source in new_sources:
            try:
                self._do_record(session, new_source)
                self._mark_as_recorded(new_source)
                record_success += 1
            except Exception as ex:
                self.logger.error('Error recording file %s. Error: %s.',
                                  new_source.get_source(), str(ex))

        record_fail = len(new_sources) - record_success

        return (record_success, record_fail)

    def submit_files(self, session):
        """
        Submit RECORDED files.
        Submission might come from new files or resubmissions

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object

        Returns:
          A tuple of int representing the number of jobs successfully submitted and the number
               of jobs not successfully submitted
        """
        self.logger.debug('Search for RECORDED files that need to be transfered')
        count_success = 0
        count_fail = 0
        records = (
            session.query(Transfer)
            .filter_by(is_invalid=0)
            .filter_by(src_host=self.host)
            .filter_by(status='RECORDED')
            .order_by(Transfer.updated_at.asc())
            .limit(self.new_files_limit))
        if records.count() > 0:
            for record in records:
                session.expunge_all()
                new_file = SourceController(self.host, record.get_source_path())

                try:
                    # Submit the file with FTS and update the record with the job_id
                    (job_id, database_id) = self._submit(session, new_file, record)
                    count_success += 1
                    self.logger.info('%s submitted', new_file.get_source())
                except Exception as ex:
                    count_fail += 1
                    self.logger.error('Error submitting %s %s', new_file.get_source(), ex)
        else:
            self.logger.debug("No new files found for submission.")

        return (count_success, count_fail)

    def _submit(self, session, source, record=None, overwrite=False):
        """
        Submit a job and store the job_id in the database

        Function supposed to be used internally (private)

        Todo:
          - Returning the database_id is not important as the record is already
              registered in the database

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object
          source (:obj:`controllers.SourceController`): The object of the file
          record (:obj:`models.Transfer`): The database entry associated to the file
          overwrite (bool, optional): Schedule the the transfer with or without overwrite.
              Defaults to False.
          metadata_msg (str, optional): Attach a message to the FTS transfer.

        Returns:
          A tuple of int representing the job_id and the database_id

        Raises:

        """
        attempt = record.transfer_attempts + 1
        metadata = {'DTO': 'submission', 'attempt': 1}
        if attempt > 1:
            metadata = {'DTO': 'resubmission', 'attempt': attempt}

        jump_destination = source.get_jump_full_destination()

        if jump_destination is not None:
            multi_hop_steps = [jump_destination]
        else:
            multi_hop_steps = []

        try:
            job_id = self.fts_instance.submit(
                source.get_fts_source(),
                source.get_fts_destination(),
                multi_hop_steps=multi_hop_steps,
                metadata=metadata,
                overwrite=overwrite
            )
        except Exception as ex:
            self.logger.error('Cannot submit %s %s', source.get_source(), ex)
            raise

        try:
            record.job_id = job_id
            record.transfer_attempts = attempt
            record.status = "SUBMITTED"
            record.dst_path = source.get_destination()
            record.jump_path = source.get_jump_destination()
            record.updated_at = datetime.utcnow()

            session.merge(record)
            session.commit()

            database_id = record.id
        except Exception as ex:
            session.rollback()
            self.logger.error('Error cannot add the job_id: %s of file %s to the database. %s',
                              job_id, source.get_source(), str(ex))
            raise
        finally:
            # release the connection to the pool
            session.close()

        return (job_id, database_id)

    def _resubmit(self, session, source, record, overwrite=False):
        """
        Resubmit a failed Job

        Function supposed to be used internally (private)

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object
          source (:obj:`controllers.SourceController`): The object of the file
          record (:obj:`models.Transfer`): The database entry associated to the file
          overwrite (bool, optional): Schedule the the transfer with or without overwrite.
              Defaults to False.

        Returns:
          An int representing the database id of the new recorded db entry

        Raises:

        """
        try:
            new_record = self._do_record(session, source,
                                         transfer_attempts=record.transfer_attempts)
        except Exception as ex:
            self.logger.error('Unable to resubmit %s. %s', source.get_source(), str(ex))
            raise

        try:
            record.resubmit_id = new_record.id
            session.merge(record)
            session.commit()
            self.logger.info("Commit on the DB %s resubmit_id", record.resubmit_id)
        except Exception as ex:
            session.rollback()
            self.logger.error('Failed Commit on the DB for resubmit_id %s. %s',
                              record.resubmit_id, ex)
            raise
        finally:
            self.logger.info('Resubmitted file %s with old job_id: %s.',
                             source.get_source(), record.id)
        return new_record.id

    def do_resubmit(self, session, minutes=60):
        """
        Resubmit a FAILED job

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object
          minutes (int, optional): number of minutes to wait before resubmitting
            Default 60 minutes.

        Returns:
          An int representing the number of resubmissions
        """
        resubmit_after_timestamp = datetime.utcnow() - timedelta(minutes=int(minutes))
        self.logger.info('Delete files remotely before UTC: %s', resubmit_after_timestamp)

        records = (
            session.query(Transfer)
            .filter_by(is_invalid=0)
            .filter_by(src_host=self.host)
            .filter_by(status='FAILED')
            .filter_by(resubmit_id=None)
            .order_by(Transfer.updated_at.desc())
            .filter(Transfer.updated_at < resubmit_after_timestamp)
            .limit(self.limit_resubmission))
        resubmit_count = 0

        # now resubmit the failed transfers
        if records.count() > 0:
            for record in records:

                filepath = record.get_jump_path()

                # clean the jump server first
                if record.jump_path is not None:
                    self.logger.info("Jump_path is not empty. Cleaning jump server of file: %s",
                                     filepath)

                    # unlink will return False if the file doesn't exist
                    if self.eos_storage.unlink(filepath):
                        try:
                            record.is_jump_file_deleted = 1
                            record.jump_file_deleted_at = datetime.utcnow()
                            session.merge(record)
                            session.commit()
                            self.logger.info("Commit on the DB %s is_jump_file_deleted",
                                             record.job_id)
                        except Exception as ex:
                            session.rollback()
                            self.logger.error("Failed Commit on the DB %s is_jump_file_deleted "
                                              "exception. %s", record.job_id, str(ex))
                    else:
                        self.logger.info("File %s does not exist on jump server", filepath)

                else:
                    self.logger.info("jump_path is empty. Cleaning of file: %s on jump server "
                                     "skipped", filepath)

                self.logger.info('Resubmitting: %s', record.get_source_path())

                source = SourceController(self.host, record.get_source_path())
                # Protecting by uncontrolled deleted sources
                if source.exists():
                    try:
                        self._resubmit(session, source, record)  # No overwrite
                        resubmit_count += 1
                    except Exception as ex:
                        self.logger.error('Error resubmitting %s %s', record.get_source_path(), ex)
                        continue
                else:
                    # Invalidating
                    self.logger.error('Error File does not exist on the source host: %s',
                                      record.get_source_path())
                    try:
                        record.is_invalid = 1
                        record.comment = "File doesn't exist on the source host"
                        session.merge(record)
                        session.commit()
                    except Exception as ex:
                        session.rollback()
                        self.logger.error('Error cannot invalidate the log on the Database: '
                                          '%s. %s', record.get_source_path(), ex)
                        continue

            # release the connection to the pool
            session.close()
        else:
            self.logger.info('No file to resubmit')
        return resubmit_count

    def do_overwrite(self, session, amount=1):
        """
        Resubmit with overwrite true in case the remote file size is 0
        This function use a wrapper around Castor to understand if the remote
        file exist and the size is 0

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object
          amount (int, optional): number of file to overwrite for function call.
            Default 1.

        Returns:
          An int representing the number of files overwritten
        """
        records = (
            session.query(Transfer)
            .filter_by(is_invalid=0)
            .filter_by(src_host=self.host)
            .filter_by(status='FAILED')
            .filter_by(resubmit_id=None)
            .filter(Transfer.transfer_attempts >= 3)
            .order_by(Transfer.transfer_attempts.desc())
            .limit(amount))

        overwrite_count = 0
        if records.count() > 0:
            for record in records:
                # stackoverflow.com/questions/8253978/sqlalchemy-get-object-not-bound-to-a-session
                # If you want a bunch of objects produced by querying a session to be usable outside
                # the scope of the session, you need to expunge them for the session.
                session.expunge_all()
                source = SourceController(self.host, record.get_source_path())
                # Protecting by uncontrolled deleted sources
                if not source.exists():
                    # Invalidating
                    self.logger.error('Error File doesnt exist on the source host')
                    try:
                        record.is_invalid = 1
                        record.comment = 'File doesnt exist on the source host'
                        session.merge(record)
                        session.commit()
                    except Exception as ex:
                        session.rollback()
                        self.logger.error('Failed to commit on the database. %s', ex)
                    continue

                dst = record.get_destination_path()
                if not self.cta.file_exists(dst):
                    self.logger.error("File doesn't exist on the remote host with why "
                                      "I am overwriting? %s", dst)
                    continue

                remote_size = self.cta.get_size(dst)
                database_size = record.file_size
                local_size = source.get_size()
                if self.cta.is_size_zero(dst):
                    if database_size == local_size:
                        # Cases where the file has 0 size on CTA
                        self.logger.info('File suitable for overwrite: %s', dst)
                        try:
                            # overwrite
                            result = self._resubmit(session, source, record, overwrite=True)
                            if result is not None:
                                overwrite_count += 1
                        except Exception as ex:
                            self.logger.error(
                                'Failed resubmission with overwrite file: %s. %s', dst, ex)
                            continue
                    else:
                        self.logger.error('File not suitable for overwrite: %s', dst)
                        continue
                else:
                    # Cases where the file has the same size on database mergers and CTA
                    # it must be a checksum error
                    all_sizes_are_equal = local_size == database_size and \
                                          database_size == remote_size
                    if all_sizes_are_equal:
                        self.logger.info('File suitable for overwrite: %s. All sizes match: %s',
                                         dst, remote_size)
                        try:
                            # overwrite
                            self._resubmit(session, source, record, overwrite=True)
                            overwrite_count += 1
                        except Exception as ex:
                            self.logger.error(
                                'Failed resubmission with overwrite file: %s. %s', dst, ex)
                            continue
                    else:
                        ## In those cases file has been recreated with the same name..
                        self.logger.error('Sizes mismatch %s', dst)
        else:
            self.logger.info('No record to overwrite!')

        # release the connection to the pool
        session.close()
        return overwrite_count


    def fetch_transfer_status(self, session):
        """
        Fetch for transfer status of submitted file
        This method understands whether the job is not anymore on the proxy DB
        Files are queried one by one
        """
        self.logger.debug('Fetch transfers status')

        count_finished = 0
        count_failed = 0
        records = (
            session.query(Transfer)
            .filter_by(is_invalid=0)
            .filter_by(src_host=self.host)
            .filter_by(status='SUBMITTED')
            .filter_by(resubmit_id=None)
            .order_by(Transfer.updated_at.asc())
            .limit(100))
        if records.count() > 0:
            for record in records:
                self.logger.debug('Fetching transfer status of ' + record.job_id +
                                  ' ' + record.get_source_path())

                try:
                    job_status = self.fts_instance.get_status(record.job_id)
                    self.logger.info(record.get_source_path() + ' status: ' + job_status[1])

                except StatusUnknown:
                    # Cannot retrieve the status, the job was removed from the DB
                    # I set it as failed
                    try:
                        record.status = 'FAILED'
                        record.updated_at = datetime.now()
                        session.merge(record)
                        session.commit()
                        self.logger.debug("Commit on the DB %s transferred", record.job_id)
                    except Exception as ex:
                        session.rollback()
                        self.logger.error("Failed to Commit on the DB job_id %s. %s",
                                          record.job_id, str(ex))
                    continue
                except Exception as ex:
                    self.logger.error('Error retrieving status of job: %s', record.job_id)
                    continue

                if job_status[1] in ['CANCELED']:
                    # I store the CANCELED one as FAILED otherwise I need to
                    # rewrite all the select queries
                    job_status[1] = 'FAILED'

                if job_status[1] in ['FINISHED', 'FAILED']:
                    try:
                        record.status = job_status[1]
                        record.transferred_at = datetime.now()
                        session.merge(record)
                        session.commit()
                        #self.logger.debug("Commit on the DB %s transferred", record.job_id)
                        self.logger.info("Commit on the DB %s transferred", record.job_id)
                    except Exception as ex:
                        session.rollback()
                        self.logger.error(
                            "Failed to Commit on the DB job_id %s. %s", record.job_id, str(ex))

                    if job_status[1] == 'FINISHED':
                        count_finished += 1
                    elif job_status[1] == 'FAILED':
                        count_failed += 1
                else:
                    # I'm not interested in other status
                    continue
        # release the connection to the pool
        session.close()
        return (count_finished, count_failed)


    def fetch_transfer_statuses(self, session):
        """
        Fetch the FTS transfer status of submitted files
        This method doesn't understand whether the job is not anymore on the proxy DB
        Files are queried in group.

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object

        Returns:
          A tuple of int representing the number of job finisched and the number
            number of job failed
        """
        self.logger.debug('Fetch transfers status')

        records = (
            session.query(Transfer)
            .filter_by(is_invalid=0)
            .filter_by(src_host=self.host)
            .filter_by(status='SUBMITTED')
            .filter_by(resubmit_id=None)
            .order_by(Transfer.updated_at.asc())
            .limit(self.fetch_limit))

        count_finished = 0
        count_failed = 0
        if records.count() > 0:
            # display the records
            ids = []
            for record in records:
                self.logger.debug('Fetching transfer status of %s %s',
                                  record.job_id, record.get_source_path())
                # extract the job_id
                ids.append(record.job_id)

            job_statuses = []
            try:
                # fetch the statuses of the all ids in the list
                # return value is a list of tuples (job_id, status)
                job_statuses = self.fts_instance.get_statuses(ids)
            except Exception as ex:
                self.logger.error('Error retrieving status of jobs: %s %s', ids, ex)
                # Cannot fetch the status of the tranfer return
                return (0, 0)

            for (job_id, job_status) in job_statuses:

                # display the status
                self.logger.info('%s transfer-status: %s', job_id, job_status)

                if job_status in ['CANCELED']:
                    # I store the CANCELED one as FAILED otherwise I need to
                    # rewrite all the select queries
                    job_status = 'FAILED'

                # I update the updated_at field for all the files
                # to check always the oldest
                updates = {"updated_at": datetime.utcnow()}

                if job_status in ['FINISHED', 'FAILED']:
                    updates["status"] = job_status
                    # storing at what time the transfer is over for FINISHED and FAILED/CANCELED
                    updates["transferred_at"] = datetime.utcnow()

                try:
                    session.query(Transfer).filter(Transfer.job_id == job_id).update(updates)
                    session.commit()
                    self.logger.debug("Job id %s status successfully updated on the DB", job_id)
                except Exception as ex:
                    session.rollback()
                    self.logger.error(
                        "Failed to Commit on the DB job_id %s exception message %s",
                        job_id, str(ex))
                    continue

                if job_status == 'FINISHED':
                    count_finished += 1
                elif job_status == 'FAILED':
                    count_failed += 1

        # release the connection to the pool
        session.close()
        return (count_finished, count_failed)

    def fetch_tape_status(self, session, interval_between_checks_minutes=60):
        """
        Fetch the tape status of transferred files

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object
          interval_between_checks_minutes (int, optional): number of minutes to wait before
            check again the tape status. Default 60 minutes.

        Returns:

        """
        allow_fetch_tape_status_before = datetime.utcnow() - \
                                         timedelta(minutes=interval_between_checks_minutes)
        self.logger.debug('Check if completed transfers are on tape')

        records = (
            session.query(Transfer)
            .filter_by(is_invalid=0)
            .filter_by(status='FINISHED')
            .filter_by(src_host=self.host)
            .filter_by(is_on_tape=0)
            .filter(Transfer.updated_at < allow_fetch_tape_status_before)
            .order_by(Transfer.on_tape_at.asc())
            .limit(100)
        )
        if records.count() > 0:
            for record in records:
                if self.cta.is_on_tape(record.get_destination_path()):
                    self.logger.info('%s tape-status: ONTAPE', record.get_destination_path())
                    try:
                        record.is_on_tape = 1
                        record.on_tape_at = datetime.utcnow()
                        session.merge(record)
                        session.commit()
                    except Exception as ex:
                        session.rollback()
                        self.logger.error("Failed to Commit on the DB bool is_on_tape "
                                          "job_id %s. %s", record.job_id, str(ex))
                else:
                    # store the time of the last is on tape check
                    self.logger.info('%s tape-status: NOTONTAPE', record.get_destination_path())
                    try:
                        record.on_tape_at = datetime.utcnow()
                        session.merge(record)
                        session.commit()
                    except Exception as ex:
                        session.rollback()
                        self.logger.error("Failed to Commit on the DB bool is_on_tape "
                                          "job_id %s. %s", record.job_id, str(ex))
        else:
            self.logger.info('No completed transfer was migrated on tape')
        session.close()

    def delete_files(self, session, life_in_minutes, check_tape=1):
        """
        Deletes the files successfully transferred

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object
          life_in_minutes (int): number of minutes to wait before deleting the file
          check_tape (int): 0,1 check or not the tape

        Returns:
          An int representing the number of files deleted
        """
        allow_delete_before = datetime.utcnow() - timedelta(minutes=life_in_minutes)
        self.logger.info('Delete files before UTC: %s', allow_delete_before)
        records = (
            session.query(Transfer)
            .filter_by(is_invalid=0)
            .filter_by(src_host=self.host)
            .filter_by(status='FINISHED')
            .filter_by(is_src_deleted=0)
            .filter_by(is_on_tape=check_tape)
            .order_by(Transfer.created_at.asc())
            .filter(Transfer.created_at < allow_delete_before)
            .limit(100))

        count_delete = 0
        if records.count() > 0:
            for record in records:
                new_source = SourceController(self.host, record.get_source_path())
                self.logger.info('Deleting: %s', record.get_source_path())
                new_source.delete()
                try:
                    record.is_src_deleted = 1
                    record.deleted_at = datetime.utcnow()
                    session.merge(record)
                    session.commit()
                    self.logger.debug("Commit on the DB %s is_source_deleted", record.job_id)
                    count_delete += 1
                except Exception as ex:
                    session.rollback()
                    self.logger.error("Failed Commit on the DB %s is_source_deleted "
                                      "exception. %s", record.job_id, str(ex))
        else:
            self.logger.info("No files to delete")

        # release the connection to the pool
        session.close()
        return count_delete

    def cleanup_jump_server(self, session, jump_cleanup_time):
        """
        Deletes the files from the jump server
        Set is_jump_file_deleted = 1 only if the file
        has been deleted from the jump server
        It checks only the files where jump_path != ""

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object
          jump_cleanup_time (int): number of minutes to keep the file on the jump server

        Returns:

        """
        allow_delete_before = datetime.utcnow() - timedelta(minutes=jump_cleanup_time)
        self.logger.info('Delete files remotely before UTC: %s', allow_delete_before)
        records = (
            session.query(Transfer)
            .filter_by(is_invalid=0)
            .filter_by(is_jump_file_deleted=0)
            .filter_by(status='FINISHED')
            .filter_by(src_host=self.host)
            .order_by(Transfer.created_at.asc())
            .filter(Transfer.created_at < allow_delete_before)
            .filter(Transfer.jump_path.isnot(None))
            .limit(100))

        if records.count() > 0:
            for record in records:

                filepath = record.get_jump_path()

                if self.eos_storage.unlink(filepath):
                    try:
                        record.is_jump_file_deleted = 1
                        record.jump_file_deleted_at = datetime.utcnow()
                        session.merge(record)
                        session.commit()
                        self.logger.info("Commit on the DB %s is_jump_file_deleted", record.job_id)
                    except Exception as ex:
                        session.rollback()
                        self.logger.error("Failed Commit on the DB %s is_jump_file_deleted "
                                          "exception. %s", record.job_id, str(ex))
                else:
                    self.logger.info("File %s does not exist on jump server or an error occurred",
                                     filepath)
        else:
            self.logger.info("No files to delete remotely")

        # release the connection to the pool
        session.close()


    def is_disk_full(self, file_dir):
        """
        Check if the disk occupancy goes beyond a threshold

        Args:
          source_dir (str): The absolute path of the directory

        Returns:
          A bool full or not full
        """
        threshold = 0.90
        statvfs = os.statvfs(file_dir)
        disk_occupancy = 1 - float(statvfs.f_bfree) / float(statvfs.f_blocks)
        if disk_occupancy > threshold:
            self.logger.error("Disk occupancy: %s", disk_occupancy)
            return True
        return False

    def delete_over_threshold(self, session, file_dir, check_tape):
        """
        Deletes file transferred but not on tape

        Args:
          session (:obj:`sqlalchemy.orm.session.Session`): Session object
          source_dir (str): The absolute path of the directory
          check_tape (int): 0,1 check or not the tape

        Returns:

        """
        number_of_cycle = 0
        # if self.is_disk_full(file_dir):
        while self.is_disk_full(file_dir) and number_of_cycle < 15:
            number_of_cycle += 1
            # Check first for files on tape non deleted because of the timeout
            records = (
                session.query(Transfer)
                .filter_by(is_invalid=0)
                .filter_by(src_host=self.host)
                .filter_by(status='FINISHED')
                .filter_by(is_src_deleted=0)
                .filter_by(is_on_tape=check_tape)
                .order_by(Transfer.created_at.asc())
                .limit(100)
            )
            count_deleted_on_tape = 0
            count_deleted_not_on_tape = 0
            if records.count() > 0:
                for record in records:
                    self.logger.info("Deleting Source on tape: %s", record.get_source_path())
                    new_source = SourceController(self.host, record.get_source_path())
                    new_source.delete()
                    try:
                        record.is_src_deleted = 1
                        record.deleted_at = datetime.utcnow()
                        session.merge(record)
                        session.commit()
                        self.logger.info("Commit on the DB %s is_source_deleted", record.job_id)
                        count_deleted_on_tape += 1
                    except Exception as ex:
                        session.rollback()
                        self.logger.error("Failed Commit on the DB %s is_source_deleted "
                                          "exception. %s", record.job_id, str(ex))
                    break

            if count_deleted_on_tape == 0:
                # I need to delete data non stored on tape
                records = (
                    session.query(Transfer)
                    .filter_by(is_invalid=0)
                    .filter_by(src_host=self.host)
                    .filter_by(status='FINISHED')
                    .filter_by(is_src_deleted=0)
                    .filter_by(is_on_tape=0)
                    .order_by(Transfer.created_at.asc())
                    .limit(100)
                )
                if records.count() > 0:
                    for record in records:
                        self.logger.info("Deleting Source not on tape: %s",
                                         record.get_source_path())
                        new_source = SourceController(self.host, record.get_source_path())
                        new_source.delete()
                        try:
                            record.is_src_deleted = 1
                            record.deleted_at = datetime.utcnow()
                            record.comment = 'File has been deleted when was not on tape'
                            session.merge(record)
                            session.commit()
                            self.logger.info("Commit on the DB %s is_source_deleted", record.job_id)
                            count_deleted_not_on_tape += 1
                        except Exception as ex:
                            session.rollback()
                            self.logger.error("Failed Commit on the DB %s is_source_deleted "
                                              "exception. %s", record.job_id, str(ex))
                        break

                    if count_deleted_not_on_tape == 0:
                        self.logger.error("Cannot delete any file!")

            self.logger.info("number of cycle %s  Sleeping..", number_of_cycle)
            time.sleep(1)
