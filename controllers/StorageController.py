# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import logging
import gfal2

class StorageController:
    """
    Allow to check the file status using the gfal2 library
    """

    def __init__(self, endpoint):
        self.logger = logging.getLogger(__name__)

        def versiontuple(v):
            return tuple(map(int, (v.split("."))))

        self.logger.info('Using Gfal2 version %s', gfal2.__version__)
        if versiontuple(gfal2.__version__) < versiontuple('1.9.5'):
            self.logger.warning('Gfal2 version %s might not work with CTA. Gfal2 version '
                                '>= 1.9.5 is required', gfal2.__version__)

        self.context = gfal2.creat_context()
        self.endpoint = endpoint
        self.error_template = "An exception of type {0} occurred. Arguments: {1}"

    def get_complete_url(self, path):
        """
        Return the complete file url: endpoint + file path
        srm url: srm://srm-public.cern.ch:8443/srm/managerv2?SFN + path
        xroot url: root://eosctapublic.cern.ch/ + path

        Args:
          path (str): The absolute path of the directory

        Returns:
          A string the complete path of a file
        """
        # Castor
        #return 'srm://srm-public.cern.ch:8443/srm/managerv2?SFN=' + path
        # CTA
        #return 'root://eosctapublic.cern.ch/' + path
        return self.endpoint + path

    def ls(self, path):
        """
        Return the list of files in a directory

        Args:
          path (str): The absolute path of the directory

        Returns:
          list (:obj:`list` of string): the list of files inside the directory
        """
        try:
            return self.context.listdir(self.get_complete_url(path))
        except gfal2.GError as error:
            message = self.error_template.format(type(error).__name__, error.message)
            self.logger.debug('Cannot list the files in the directory: %s %s. %s',
                              path, error.code, message)
            return []

    def file_exists(self, path):
        """
        Check if a file or a directory exists

        Args:
          path (str): The absolute path of the file

        Returns:
          An bool, true if a file exist
        """
        try:
            self.context.lstat(self.get_complete_url(path))
            self.logger.debug('File exists: %s', path)
            return True
        except gfal2.GError as error:
            message = self.error_template.format(type(error).__name__, error.message)
            self.logger.debug('File does not exist: %s %s. %s', path, error.code, message)
            return False

    def unlink(self, path):
        """
        Unlink (remove) a file, directories cannot be unlink

        Args:
          path (str): The absolute path of the file to unlink

        Returns:
          An bool, true on success
        """
        if not self.file_exists(path):
            self.logger.warning('Must be an existing file %s', path)
            return False

        try:
            self.context.unlink(self.get_complete_url(path))
            # print("Doing unlink: " + self.get_complete_url(path))
            return True
        except gfal2.GError as error:
            message = self.error_template.format(type(error).__name__, error.message)
            self.logger.error('Cannot unlink the file: %s %s. %s', path, error.code, message)
            return False

    def mv(self, path1, path2):
        """
        Move a file or a directory from path1 to path2

        Args:
          path1 (str): The absolute path of the file to move
          path2 (str): The absolute path of the destination

        Returns:
          An bool, true on success
        """
        if not self.file_exists(path1):
            return False
        if self.file_exists(path2):
            return False

        try:
            self.context.rename(self.get_complete_url(path1), self.get_complete_url(path2))
            return True
        except gfal2.GError as error:
            message = self.error_template.format(type(error).__name__, error.message)
            self.logger.error('Cannot move the file: %s to %s. %s %s',
                              path1, path2, error.code, message)
            return False

    def get_size(self, path):
        """
        Returns the size information of a file

        Other variables that can be accessed (append st_ in front):
        uid: 47
        gid: 48
        mode: 100644
        size: 3946634256
        nlink: 1
        ino: 0
        ctime: 1502717573
        atime: 0
        mtime: 1502717573

        Args:
          path (str): The absolute path of the file

        Returns:
          An int representing the file size in bytes
        """

        try:
            return self.context.stat(self.get_complete_url(path)).st_size
        except gfal2.GError as error:
            message = self.error_template.format(type(error).__name__, error.message)
            self.logger.error('Error while getting the file info %s %s. %s',
                              path, error.code, message)

    def is_size_zero(self, path):
        """
        Returns true if the file size is 0

        Args:
          path (str): The absolute path of the file

        Returns:
          An bool, true if the file size is equal to zero
        """
        return self.get_size(self.get_complete_url(path)) == 0
