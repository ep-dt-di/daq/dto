# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

"""App configuration."""
from os import environ, path
import logging
from dotenv import load_dotenv

logger = logging.getLogger(__name__)

def load_configuration_file(base_dir=path.abspath(path.dirname(__file__))):
    configuration_file = path.join(base_dir, '.env')
    if not path.exists(configuration_file):
        logger.error('Cannot find configuration file: %s', configuration_file)
        return False
    logger.info('Loading configuration file: %s', configuration_file)
    load_dotenv(configuration_file)
    return True

def check_string_option(option):
    tmp = environ.get(option)
    if tmp is None or tmp == '':
        logger.error("%s has an invalid value: %s", option, str(tmp))
        return False
    return True

def check_int_option(option):
    tmp = environ.get(option)
    if tmp is None or tmp == '':
        logger.error("%s has an invalid value: %s", option, str(tmp))
        return False

    try:
        int_value = int(tmp)
    except ValueError as ex:
        logger.error("%s has an invalid value: %s. "
                     "Please set a numerical value!", option, str(tmp))
        return False
    return True


def validate_configuration():
    """
    Check the configuration
    """

    invalid_values = []

    string_options = ['host', 'endpoint', 'file_dirs',
                      'base_destination', 'protocol_base', 'regex',
                      'source_owner', 'source_group',
                      'new_source_group', 'db_user', 'db_host',
                      'db_port', 'db_name']
    for option in string_options:
        if not check_string_option(option):
            invalid_values.append(option)

    int_options = ['minutes_of_life', 'sleep_time', 'check_tape', 'regex_group_index',
                   'new_files_limit', 'limit_resubmission', 'fetch_limit',
                   'resubmit_after_minutes']
    for option in int_options:
        if not check_int_option(option):
            invalid_values.append(option)

    if environ.get('source_group') == environ.get('new_source_group'):
        invalid_values.append('new_source_group')
        logger.error('source_group and new_source_group must be different')

    password = environ.get('db_pass')
    if password is None:
        invalid_values.append('db_pass')
        logger.error("db_pass has an invalid value: %s", str(password))

    if len(invalid_values) > 0:
        logger.error("These options %s are not correctly configured!", invalid_values)
        return False
    return True

def get_list_from_string(configuration_string):
    """
    Converts the configuration string in a list of directories
    Coma separator
    """
    if configuration_string == "":
        return []
    return list(map(str.strip, configuration_string.split(',')))
