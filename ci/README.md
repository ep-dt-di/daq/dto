# Continuous Integration tests
The default location of the CI file is:

    .gitlab-ci.yml

In this project has been changed to:

    ci/.gitlab-ci.yml

The path can be changed in: Settings - CI/CD - General pipelines - Custom CI configuration path

All the files related to the CI are stored in the `ci/` directory.


## CI Tests
The CI chain is divided in 5 stages:

- *build-docker-image* builds the DTO Docker image and push it to the repository container registry. It takes some time to build the images and usually is not something that you want to change often, for that reason the image is build only if there is any change in the corresponding Dockerfile image. Otherwise the image build is done when the pipeline is triggered manually.
- *db-tests* the db tests are meant to test the Alembic migrations against different database versions (MariaDB and Mysql). The Alembic migration is executed in the Docker container produced in the previous stage.
- *tests* this stage is meant to execute Pylint, Unit and functional tests. Tests have to run in the Docker image produced in the first stage, and if needed have to attach the database service.
- *c8 db-tests* same as db-test but with CentOS8 and Python3
- *c8 tests* same as db-test but with CentOS8 and Python3

## Database tests
There are some table schema incompatibilities with the newest versions of MariaDb and Mysql.
The database tests are successfull with the following versions:

- *MariaDB*: 5.5, 10.0, 10.1, 10.2, 10.3
- *Mysql*: 5.6

## Test environment
- CentOS7 Docker container
- MariaDB 5.5
- Python 2.7
