#!/usr/bin/env python3

from pathlib import Path
import requests
import sys

# seems that a token is required now...
# .gitlab-ci.yml Syntax check
#https://docs.gitlab.com/ee/api/lint.html

ci_file = Path('./ci/.gitlab-ci.yml')
if len(sys.argv) > 1:
    p = Path(sys.argv[1])
    if not p.is_file():
        print("File not exist")
        sys.exit(0)

    ci_file = p

f = open(ci_file, "r")
gitlab_file = f.read()

#url = "https://gitlab.com/api/v4/ci/lint"
url = "https://gitlab.cern.ch/api/v4/ci/lint"
data = {'content': gitlab_file}
r = requests.post(url, json=data)
#print(r.status_code)
#print(r.json())
if r.status_code == 200 and r.json()['status'] == 'valid':
    print(str(ci_file) + ' syntax ok!')
    sys.exit(0)

print(str(ci_file) + ' broken')
print(r.content)
sys.exit(1)

