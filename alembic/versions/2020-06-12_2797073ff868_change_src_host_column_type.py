# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

"""change src_host column type and fix typo

Revision ID: 2797073ff868
Revises: fde0be5859f7
Create Date: 2020-06-12 07:25:00.738569

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '2797073ff868'
down_revision = 'fde0be5859f7'
branch_labels = None
depends_on = None

old_enum_type = sa.Enum('na62merger1', 'na62merger2', 'na62merger3', 'na62merger4', 'na62merger5',
                     'na62primitive', name='src_host')
new_type = sa.String(255)
old_enum_type_empty_string = sa.Enum('na62merger1', 'na62merger2', 'na62merger3', 'na62merger4',
                                  'na62merger5', 'na62primitive', '', name='src_host')


def upgrade():
    op.alter_column('transfers', 'src_host', nullable=False, type_=new_type,
                    existing_type=old_enum_type, server_default="")
    op.alter_column('transfers', 'transfer_attemp', existing_type=sa.Integer, nullable=False,
                    new_column_name='transfer_attempts')


def downgrade():

    op.alter_column('transfers', 'transfer_attempts', existing_type=sa.Integer, nullable=False,
                    new_column_name='transfer_attemp')

    # Avoids to trigger the exception sa.exc.DataError when some rows are in the DB
    conn = op.get_bind()
    conn.execute(
        sa.sql.text(
            """
                UPDATE transfers
                SET src_host = 'na62merger1'
            """
        )
    )
    op.alter_column('transfers', 'src_host', nullable=False, type_=old_enum_type_empty_string,
                        existing_type=new_type, server_default="")
