# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

"""create transfers table

Revision ID: fc68b24bc274
Revises: Marco Boretto marco.boretto@cern.ch
Create Date: 2017-04-07 08:58:01.335411

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'fc68b24bc274'
down_revision = None
branch_labels = None
depends_on = None

def upgrade():
    op.create_table(
        'transfers',
        sa.Column('id', sa.dialects.mysql.BIGINT(unsigned=True), primary_key=True),
        sa.Column('file_name', sa.String(255), nullable=False),
        sa.Column('src_host',
                  sa.Enum('na62merger1', 'na62merger2', 'na62merger3', 'na62merger4', 'na62merger5',
                       'na62primitive'), nullable=False),
        sa.Column('src_path', sa.String(255), nullable=False),
        sa.Column('dst_path', sa.String(255)),
        sa.Column('status',
                  sa.Enum('SUBMITTED', 'ACTIVE', 'FINISHED', 'READY', 'STAGING', 'STARTED', 'FAILED'),
                  default=1, nullable=False),
        sa.Column('job_id', sa.String(255)),
        sa.Column('created_at', sa.TIMESTAMP, nullable=False,
                  server_default=sa.sql.func.current_timestamp()),
        sa.Column('updated_at', sa.TIMESTAMP, nullable=False,
                  server_default=sa.text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")),
        sa.Column('transferred_at', sa.TIMESTAMP, nullable=True),
        sa.Column('file_size', sa.dialects.mysql.BIGINT(unsigned=True)),
        sa.Column('is_src_deleted', sa.Boolean, nullable=False, default=0),
        sa.Column('deleted_at', sa.TIMESTAMP, nullable=True),
        sa.Column('transfer_attemp', sa.Integer, nullable=False),
        sa.Column('resubmit_id', sa.dialects.mysql.BIGINT(unsigned=True), sa.ForeignKey("transfers.id"), default=None),
        sa.Column('is_invalid', sa.Boolean, nullable=False, default=0),
        sa.Column('comment', sa.String(255), nullable=True, default=None),
    )

    op.create_index('ik_status_per_host', 'transfers',
                    ['is_invalid', 'src_host', 'status', 'is_src_deleted'])
    op.create_index('ik_failed_job', 'transfers',
                    ['is_invalid', 'src_host', 'status', 'resubmit_id'])
    op.create_index('ik_attemp_per_host', 'transfers', ['src_host', 'transfer_attemp'])
    op.create_index('ik_job_id', 'transfers', ['job_id'])
    op.create_index('ik_is_src_deleted', 'transfers', ['is_src_deleted'])
    op.create_index('ik_transfer_attemp', 'transfers', ['transfer_attemp'])
    op.create_index('ik_resubmit_id', 'transfers', ['resubmit_id'])
    op.create_index('ik_is_invalid', 'transfers', ['is_invalid'])


def downgrade():
    op.drop_table('transfers')
