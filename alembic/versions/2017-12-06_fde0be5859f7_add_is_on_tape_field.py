# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

"""add is_on_tape field

Revision ID: fde0be5859f7
Revises: fc68b24bc274
Create Date: 2017-12-06 18:33:28.434000

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'fde0be5859f7'
down_revision = 'fc68b24bc274'
branch_labels = None
depends_on = None

new_enum_type = sa.Enum('SUBMITTED', 'ACTIVE', 'FINISHED', 'READY', 'STAGING', 'STARTED', 'FAILED',
                        name='status')
old_enum_type = new_enum_type


def upgrade():
    # Add constraint to file_size
    op.alter_column('transfers', 'file_size', nullable=False, type_=sa.dialects.mysql.BIGINT(unsigned=True),
                    server_default=None)
    # Others fixes
    op.alter_column('transfers', 'is_src_deleted', nullable=False, default=False,
                    type_=sa.types.BOOLEAN, server_default=sa.false())
    op.alter_column('transfers', 'is_invalid', nullable=False, default=False,
                    type_=sa.types.BOOLEAN, server_default=sa.false())
    op.alter_column('transfers', 'transfer_attemp', nullable=False, default=1,
                    type_=sa.types.Integer, server_default="1")
    op.alter_column('transfers', 'status', nullable=False, type_=new_enum_type,
                    existing_type=old_enum_type, server_default="SUBMITTED")

    op.add_column('transfers',
                  sa.Column('is_on_tape', sa.Boolean(), nullable=False, server_default=sa.false()))

    op.create_index('ik_on_tape_job', 'transfers',
                    ['is_invalid', 'src_host', 'status', 'is_on_tape'])
    op.create_index('ik_is_on_tape', 'transfers', ['is_on_tape'])

    # on_tape_at
    op.add_column('transfers', sa.Column('on_tape_at', sa.TIMESTAMP, nullable=True))


def downgrade():
    op.drop_index('ik_on_tape_job', 'transfers')

    op.drop_column('transfers', 'on_tape_at')
    op.drop_column('transfers', 'is_on_tape')

    # Missing fixes

    op.alter_column('transfers', 'status', nullable=False, type_=old_enum_type,
                        existing_enum_type=new_enum_type, server_default=None)
    op.alter_column('transfers', 'transfer_attemp', nullable=False, default=1,
                    type_=sa.types.Integer, server_default=None)
    op.alter_column('transfers', 'is_invalid', nullable=False, default=False,
                    type_=sa.types.BOOLEAN, server_default=None)
    op.alter_column('transfers', 'is_src_deleted', nullable=False, default=False,
                    type_=sa.types.BOOLEAN, server_default=None)
    # Remove constraint to file_size
    op.alter_column('transfers', 'file_size', nullable=True, type_=sa.dialects.mysql.BIGINT(unsigned=True))
