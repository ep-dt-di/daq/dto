"""add record state

Revision ID: 576ae634c607
Revises: 2797073ff868
Create Date: 2021-03-05 11:34:16.673518

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '576ae634c607'
down_revision = '2797073ff868'
branch_labels = None
depends_on = None

old_enum_type = sa.Enum('SUBMITTED', 'ACTIVE', 'FINISHED', 'READY', 'STAGING', 'STARTED', 'FAILED',
                        name='status')
new_enum_type = sa.Enum('RECORDED', 'SUBMITTED', 'ACTIVE', 'FINISHED', 'READY', 'STAGING', 'STARTED', 'FAILED',
                        name='status')


def upgrade():

    op.alter_column('transfers', 'status', nullable=False, type_=new_enum_type,
                    existing_type=old_enum_type, server_default="RECORDED")
    op.alter_column('transfers', 'transfer_attempts', nullable=False, default=0,
                    type_=sa.types.Integer, server_default="0")


def downgrade():

    op.alter_column('transfers', 'transfer_attempts', nullable=False, default=1,
                    type_=sa.types.Integer, server_default="1")

    # Avoids to trigger the exception sa.exc.DataError when some rows are in the DB
    conn = op.get_bind()
    conn.execute(
        sa.sql.text(
            """
                UPDATE transfers
                SET status = 'SUBMITTED'
                WHERE status = 'RECORDED'
            """
        )
    )

    op.alter_column('transfers', 'status', nullable=False, type_=old_enum_type,
                    existing_enum_type=new_enum_type, server_default=None)
