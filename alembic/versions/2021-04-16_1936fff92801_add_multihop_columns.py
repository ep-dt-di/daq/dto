"""add multihop columns

Revision ID: 1936fff92801
Revises: 576ae634c607
Create Date: 2021-04-16 10:30:46.825308

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '1936fff92801'
down_revision = '576ae634c607'
branch_labels = None
depends_on = None


def upgrade():

    op.add_column('transfers', sa.Column('jump_path', sa.String(255),
                  nullable=True, default=None))
    op.add_column('transfers', sa.Column('is_jump_file_deleted', sa.Boolean,
                  nullable=False, default=0))
    op.add_column('transfers', sa.Column('jump_file_deleted_at', sa.TIMESTAMP,
    	          nullable=True))

    op.create_index('ik_jump_file_deleted', 'transfers', ['is_jump_file_deleted'])


def downgrade():
    op.drop_index('ik_jump_file_deleted', 'transfers')

    op.drop_column('transfers', 'jump_path')
    op.drop_column('transfers', 'is_jump_file_deleted')
    op.drop_column('transfers', 'jump_file_deleted_at')
