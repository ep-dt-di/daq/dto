#!/usr/bin/env python3
#
# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import time
import gc
import logging.config
import traceback
from datetime import datetime

from config import load_configuration_file, validate_configuration, get_list_from_string

from controllers.DtoController import DtoController
from controllers.EosController import EosController
from controllers.FtsController import FtsController
from controllers.GracefulInterruptController import GracefulInterruptController
from models.utils import get_db_session

if __name__ == '__main__':
    DTO_LOGGING_CONFIGURATION_FILE = '/etc/dto.d/dto-logging.ini'
    LOGGING_FILE_EXIST = False
    if os.path.exists(DTO_LOGGING_CONFIGURATION_FILE):
        LOGGING_FILE_EXIST = True
        logging.config.fileConfig(DTO_LOGGING_CONFIGURATION_FILE)
    else:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(name)s (%(lineno)s) - '
                                   '%(levelname)s: %(message)s',
                            datefmt='%Y.%m.%d %H:%M:%S'
                           )

    logger = logging.getLogger(__name__)
    if not LOGGING_FILE_EXIST:
        logger.warning('Missing logging config file: %s', DTO_LOGGING_CONFIGURATION_FILE)

    if not load_configuration_file():
        sys.exit(1)
    if not validate_configuration():
        sys.exit(1)

    logger.info("Starting DTO")

    session = get_db_session(os.environ.get('db_user'),
                             os.environ.get('db_pass'),
                             os.environ.get('db_host'),
                             os.environ.get('db_port'),
                             os.environ.get('db_name')
                            )()
    logger.info('Database connection established')


    minutes_of_life = int(os.environ.get('minutes_of_life'))
    check_tape = bool(int(os.environ.get('check_tape')))
    sleep_time = int(os.environ.get('sleep_time'))
    resubmit_after_minutes = int(os.environ.get('resubmit_after_minutes'))

    try:
        jump_cleanup_time = int(os.environ.get('jump_cleanup_time'))
    except ValueError as ex:
        jump_cleanup_time = 1440
        logger.info("No value set for jump_cleanup_time. Default value of 1440 will be used.")
    jump_endpoint = os.environ.get('jump_protocol_base')

    fts_instance = FtsController(os.environ.get('endpoint'))
    if jump_endpoint != "":
        eos_storage = EosController(endpoint=jump_endpoint)
        dto = DtoController(fts_instance, os.environ.get('host'), eos_storage)
    else:
        dto = DtoController(fts_instance, os.environ.get('host'))

    file_dirs = get_list_from_string(os.environ.get('file_dirs'))

    with GracefulInterruptController() as handler:
        start_date = datetime.utcnow()
        record_success_tot = 0
        record_fail_tot = 0
        submit_success_tot = 0
        submit_fail_tot = 0
        transfer_finished_tot = 0
        transfer_failed_tot = 0
        files_deleted_tot = 0
        transfer_resubmitted_tot = 0

        cycle_count = 0
        count_since_beginning = 0

        while True:
            try:
                # the purpose of this query is to check the db status
                # in case of failure the code below will be skipped until the connection is back
                result = session.execute('SELECT version()').fetchall()

                # Look for new file in every dir
                for file_dir in file_dirs:
                    if not os.path.isdir(file_dir):
                        logger.error("%s doesn't exist", file_dir)
                        continue

                    (record_success, record_fail) = dto.record_files(session, file_dir)
                    record_success_tot += record_success
                    record_fail_tot += record_fail

                # Create a new record for file that need to be resubmitted
                transfer_resubmitted_tot += dto.do_resubmit(session,
                                                            resubmit_after_minutes)

                # Submit new files and the one that needs resubmission
                (submit_success, submit_fail) = dto.submit_files(session)
                submit_success_tot += submit_success
                submit_fail_tot += submit_fail

                (transfer_finished, transfer_failed) = dto.fetch_transfer_status(session)
                transfer_finished_tot += transfer_finished
                transfer_failed_tot += transfer_failed

                if check_tape:
                    interval_between_checks_minutes = 5
                    dto.fetch_tape_status(session, interval_between_checks_minutes)

                files_deleted_tot += dto.delete_files(session, minutes_of_life, int(check_tape))

                for file_dir in file_dirs:
                    if not os.path.isdir(file_dir):
                        logger.error("%s doesn't exist", file_dir)
                        continue
                    # occupancy based selection
                    dto.delete_over_threshold(session, file_dir, int(check_tape))

                dto.cleanup_jump_server(session, jump_cleanup_time)

                logger.info("DTO running since UTC %s", start_date)
                logger.info("Files recorded: %s", record_success_tot)
                logger.info("Files recorded fail: %s", record_fail_tot)
                logger.info("Submit fail: %s", submit_fail_tot)
                logger.info("Submitted success: %s", submit_success_tot)
                logger.info("Transfers FINISHED: %s", transfer_finished_tot)
                logger.info("Transfers FAILED:  %s", transfer_failed_tot)
                logger.info("Resubmissions: %s", transfer_resubmitted_tot)
                logger.info("Files deleted: %s", files_deleted_tot)

                cycle_count += 1
                count_since_beginning += 1

                if count_since_beginning * sleep_time > 60 * 60:
                    logger.info("Scheduled exit")
                    # automatic exit after an hour due to memory leak
                    # the service manager will restart it
                    sys.exit(1)

                if cycle_count * sleep_time > 60 * 10:
                    collected = gc.collect()
                    cycle_count = 0
                    logger.info("Garbage collector: collected %d objects.", collected)

                time.sleep(sleep_time)
            except Exception as ex:
                # that was the only way to catch this exception
                # https://mail.python.org/pipermail/python-list/2008-January/483946.html
                if ex.__class__.__name__.find('OperationalError') != -1:
                    session.rollback()
                    wait_before_reconnect = 7
                    logger.error("Database connection lost.. waiting %ss before reconnecting..",
                                 wait_before_reconnect)
                    time.sleep(wait_before_reconnect)
                else:
                    # unknown exception
                    logger.error(ex.__class__.__name__)
                    logger.error(traceback.print_exc())
                    sys.exit(1)

            if handler.interrupted:
                logger.info('DTO interrupped!')
                time.sleep(1)
                break
