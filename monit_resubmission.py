#!/usr/bin/env python3
#
# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import sys
from datetime import datetime, timedelta

from config import load_configuration_file, validate_configuration
from controllers.CtaController import CtaController
from controllers.SourceController import SourceController
from models.Transfer import Transfer
from models.utils import get_db_session

if __name__ == '__main__':

    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    logger = logging.getLogger(__name__)
    logger.info('Starting resubmission monitor')

    if not load_configuration_file():
        sys.exit(1)
    if not validate_configuration():
        sys.exit(1)

    session = get_db_session(os.environ.get('db_user'),
                             os.environ.get('db_pass'),
                             os.environ.get('db_host'),
                             os.environ.get('db_port'),
                             os.environ.get('db_name')
                            )()

    host = os.environ.get('host')
    minutes = 60 * 6
    allow_resubmit_before = datetime.utcnow() + timedelta(minutes=-int(minutes))
    logger.info('Running on: %s', host)

    records = (
        session.query(Transfer)
        .filter_by(is_invalid=0)
        .filter_by(status='FAILED')
        .filter_by(resubmit_id=None)
        .filter_by(src_host=host)
        .filter(Transfer.transfer_attempts >= 3)
        # .order_by(Transfer.updated_at.desc())
        # .filter(Transfer.updated_at < allow_resubmit_before)
        .order_by(Transfer.transfer_attempts.desc())
        .limit(1000)
    )
    resubmit_count = 0

    size_zero_count = 0
    size_non_zero_count = 0
    suitable_for_overwrite_count = 0
    fail_checksum_count = 0
    file_recreated_count = 0
    remote_dont_exist = 0

    if records.count() > 0:
        castor = CtaController()
        for record in records:
            new_source = SourceController(host, record.get_source_path())
            database_size = record.file_size
            local_size = new_source.get_size()
            if castor.file_exists(record.get_destination_path()):
                remote_size = castor.get_size(record.get_destination_path())
                if remote_size == 0:
                    size_zero_count += 1
                    if database_size == local_size:
                        suitable_for_overwrite_count += 1
                else:
                    size_non_zero_count += 1
                    if database_size == local_size and database_size == remote_size:
                        fail_checksum_count += 1
                    else:
                        file_recreated_count += 1
                logger.info('%s %s %s %s Remote Size: %s Database Size: %s LocalSize: %s',
                            record.id, record.job_id, record.transfer_attempts,
                            record.get_source_path(), remote_size, database_size, local_size)
            else:
                remote_dont_exist += 1
                logger.error('Remote file not found %s', record.get_destination_path())
                logger.error(record)
                continue

        logger.info('SUMMARY Found: %s Records', records.count())
        logger.info('Remote file doesnt exist: %s Records', remote_dont_exist)
        logger.info('Remote file_size 0: %s Records', size_zero_count)
        logger.info('Size non 0: %s Records', size_non_zero_count)

        logger.info('Suitable for overwrite:')
        logger.info('- Remote file size 0: %s Records', suitable_for_overwrite_count)
        logger.info('- Checksum failed: %s Records', fail_checksum_count)
        logger.info('To investigate:')
        logger.info('- File has been recreated after submission: %s Records', file_recreated_count)

    else:
        logger.info('No records found')
