#!/usr/bin/env python3
#
# Copyright (C) 2020 CERN
#
# DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import logging
import shutil
import time
from optparse import OptionParser

from config import validate_configuration, load_configuration_file
from models.Transfer import Transfer
from models.utils import get_db_session


def fetchLastFile():
    opts = OptionParser()
    opts.add_option('-o', '--output', dest='outputfilename', default='/opt/dto/output.list')
    opts.add_option('-u', '--userid', dest='userid', type='int', default=-1)

    (options, args) = opts.parse_args()

    # Check for  transfer status
    log.info('Check for completed transfers')
    # .filter_by(status='FINISHED')
    records = (
        session.query(Transfer)
        .filter(Transfer.src_host != 'na62primitive')
        .filter(Transfer.transfer_attempts == 1)
        .order_by(Transfer.created_at.desc())
        .limit(1)
    )

    staticfilename = "/opt/dto/lastfile.list"
    if records.count() > 0:
        for record in records:
            # print record.src_host + record.src_path + '/' + record.file_name
            path = record.src_host + '/' + record.file_name + "\n"
            # path = record.src_host + '/' + record.file_name + ' ' + str(record.created_at)
            print(path)
            newrecord = 1
            if os.path.exists(staticfilename):
                my_file = open('%s' % staticfilename, 'r')
                for line in my_file:
                    if record.file_name in line:
                        newrecord = 0
                        break
                my_file.close()
            if newrecord == 1:
                my_file = open('%s' % staticfilename, 'w')
                my_file.write(path)
                my_file.close()
                shutil.copy(staticfilename, options.outputfilename)
                os.chown('%s' % options.outputfilename, options.userid, 1338)  # gid = 1338 (vl)

    # release the connection to the pool
    session.close()


if __name__ == '__main__':
    log = logging.getLogger(__name__)
    if not load_configuration_file():
        sys.exit(1)
    if not validate_configuration():
        sys.exit(1)
    session = get_db_session(os.environ.get('db_user'),
                             os.environ.get('db_pass'),
                             os.environ.get('db_host'),
                             os.environ.get('db_port'),
                             os.environ.get('db_name')
                            )()
    while True:
        fetchLastFile()
        time.sleep(15)
    # fetchLastFile()
